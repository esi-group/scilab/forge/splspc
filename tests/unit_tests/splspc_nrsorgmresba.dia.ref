//===========================================================================//
// Copyright (C) 2011 - DIGITEO - Michael Baudin                             //
// Copyright (C) 2011 - National Institute of Informatics - Benoit Goepfert  //
//                                                                           //
// This file must be used under the terms of the CeCILL.                     //
// This source file is licensed as described in the file COPYING, which      //
// you should have received as part of this distribution.  The terms         //
// are also available at                                                     //
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                  //
//===========================================================================//
// test 1 : a small matrix (control) (overdetermined)
// A: 8-by-6 matrix with 23 nonzeros
// b: 8-by-1 matrix
A=[
-1.    3.    0.    0.    4.    0.  
 2.   -5.    0.    0.    0.    1.  
 0.    0.   -2.    3.    0.    0.  
 0.    0.    7.   -1.    0.    0.  
-3.    0.    0.    4.    6.    0.
 0.    5.    0.    0.   -7.    8.
-1     0     0     7     2     0
 0    -4     3     0     5    -9 
];
A=sparse(A);
xexp = (1:size(A,"c"))';
b = A*xexp; 
eps = 1.e-6;
maxout = 1000;
nbin=1;
omg=1.0;
nboutexp = 6;
//
x = splspc_nrsorgmresba(A,b);
assert_checkalmostequal(x,xexp,0,1D-7);
//
[x,nbout,resid] = splspc_nrsorgmresba(A,b,omg,nbin,maxout,eps);
assert_checkequal(nbout,nboutexp);
assert_checktrue(resid(nbout) < 1.e-6);
assert_checkalmostequal(x,xexp,0,1D-7);
//
[x2,nbout2,resid2] = splspc_nrsorgmresba(A,b,[],[],[],[]);
assert_checkequal(nbout,nbout2);
assert_checkequal(resid(nbout),resid2(nbout2));
assert_checkequal(x,x2);
//
[x3,nbout3,resid3] = splspc_nrsorgmresba(A,b);
assert_checkequal(nbout,nbout3);
assert_checkequal(resid(nbout),resid3(nbout3));
assert_checkequal(x,x3);
// test 1 : a small matrix (control) (underdetermined)
A=A';
m = size(A,1);
b = ones(m,1);
eps = 1.e-6;
maxout = 1000;
nbin=1;
omg=1.0;
xexp = [  0.8988740  
0.3998945  
0.2202555  
0.2248332  
-0.4857449  
0.0250176  
0.3581495  
-0.0444405 ];  
nboutexp = 5;
[x,nbout,resid] = splspc_nrsorgmresba(A,b,omg,nbin,maxout,eps);
assert_checkequal(nbout,nboutexp);
assert_checktrue(resid(nbout) < 1.e-6);
assert_checkalmostequal(x,xexp,0,1D-6);
//
[x2,nbout2,resid2] = splspc_nrsorgmresba(A,b,[],[],[],[]);
assert_checkequal(nbout,nbout2);
assert_checkequal(resid(nbout),resid2(nbout2));
assert_checkequal(x,x2);
//
[x3,nbout3,resid3] = splspc_nrsorgmresba(A,b);
assert_checkequal(nbout,nbout3);
assert_checkequal(resid(nbout),resid3(nbout3));
assert_checkequal(x,x3);
//test 2 : a large sparse matrix, illc1850 (overdetermined)
path = fullfile(splspc_getpath(),"tests","matrices");
filename = fullfile(path,"illc1850.mtx");
A = mmread(filename);
m = size(A,1);
b = rand(m,1);
nbin=4;
omg=1.4;
tol=1.e-6;
[x,nbout,resid] = splspc_nrsorgmresba(A,b,omg,nbin);
assert_checktrue(nbout < maxout);
[x2,nbiter,resid2]=splspc_gmresba(A,b);
assert_checktrue(nbout < nbiter);
assert_checkalmostequal(norm(b-A*x),norm(b-A*x2),1.e-5);
x3=A\b;
assert_checkalmostequal(norm(b-A*x),norm(b-A*x3),1.e-5);
//test 2 : a large sparse matrix, illc1850 (underdertermined)
path = fullfile(splspc_getpath(),"tests","matrices");
filename = fullfile(path,"illc1850.mtx");
A = mmread(filename);
A = A';
n = size(A,2);
e = ones(n,1);
b = A * e;
nbin=4;
omg=1.4;
tol=1.e-6;
[x,nbout,resid] = splspc_nrsorgmresba(A,b,omg,nbin);
assert_checktrue(nbout < maxout);
[x2,nbiter,resid2]=splspc_gmresab(A,b);
assert_checktrue(nbout < nbiter);
assert_checkalmostequal(norm(b-A*x),norm(b-A*x2),0,1.e-1);
//test 3 : a large sparse rank deficient matrix, Maragal_3 (overdetermined)
path = fullfile(splspc_getpath(),"tests","matrices");
filename = fullfile(path, "Maragal_3_.mtx");
A = mmread(filename);
m = size(A,1);
b = rand(m,1);
nbin=3;
omg=1.2;
tol=1.e-6;
[x,nbout,resid] = splspc_nrsorgmresba(A,b,omg,nbin);
assert_checktrue(nbout < maxout);
[x2,nbiter,resid2] = splspc_gmresba(A,b);
assert_checktrue(nbout < nbiter);
assert_checkalmostequal(norm(b-A*x),norm(b-A*x2),1.e-5);
//test 3 : a large sparse rank deficient matrix, Maragal_3 (underdetermined)
path = fullfile(splspc_getpath(),"tests","matrices");
filename = fullfile(path, "Maragal_3_.mtx");
A = mmread(filename);
A = A';
n = size(A,2);
e = ones(n,1);
b = A * e;
nbin=3;
omg=1.2;
tol=1.e-6;
[x,nbout,resid] = splspc_nrsorgmresba(A,b,omg,nbin);
assert_checktrue(nbout < maxout);
[x2,nbiter,resid2] = splspc_gmresab(A,b);
assert_checktrue(nbout < nbiter);
assert_checkalmostequal(norm(b-A*x),norm(b-A*x2),0,1.e-2);
