<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 *
 * Copyright (C) 2011 - DIGITEO - Michael Baudin
 * Copyright (C) 2011 - National Institute of Informatics - Benoit Goepfert
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry 
	xmlns="http://docbook.org/ns/docbook" 
	xmlns:xlink="http://www.w3.org/1999/xlink" 
	xmlns:svg="http://www.w3.org/2000/svg" 
	xmlns:mml="http://www.w3.org/1998/Math/MathML" 
	xmlns:db="http://docbook.org/ns/docbook" 
	version="5.0-subset Scilab" 
	xml:lang="en" 
	xml:id="splspc_grevgmresba">
	<refnamediv>
		<refname>splspc_grevgmresba</refname>
		<refpurpose>BA-GMRES iterative solver with a GREVILLE preconditioner</refpurpose>
	</refnamediv>
	<refsynopsisdiv>
		<title>Calling Sequence</title>
		<synopsis>
			x=splspc_grevgmresba(A, b)
			x=splspc_grevgmresab(A, b, tgrev)     
			x=splspc_grevgmresba(A, b, tgrev, tsgrev)        
			x=splspc_grevgmresba(A, b, tgrev, tsgrev, restart)
			x=splspc_grevgmresba(A, b, tgrev, tsgrev, restart, maxiter)
			x=splspc_grevgmresba(A, b, tgrev, tsgrev, restart, maxiter, tol)
			[x, nbiter]= splsc_grevgmresba(...)
			[x, nbiter, res]= splsc_grevgmresba(...)
		</synopsis>
	</refsynopsisdiv>
	<refsection>
		<title>Parameters</title>
		<variablelist>
			<varlistentry>
				<term>A</term>
				<listitem>
					<para>
						sparse matrix (size m x n) : the matrix A in the least square problem min(b-Ax)
					</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>b</term>
				<listitem>
					<para>
						matrix of double (size m x 1) : the vector b in the least square problem min(b-Ax)
					</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>tgrev</term>
				<listitem>
					<para>
						matrix of double (size 1 x 1) : a dropping tolerance parameter for the eliminations of the smaller elements during the algorithm (default = 1.e-3)
					</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>tsgrev</term>
				<listitem>
					<para>
						matrix of double (size 1 x 1) : a switching tolerance parameter for the detection of linearly dependants colums in the matrix A (default = 1.e-6)
					</para>
				</listitem>
			</varlistentry>   			            
			<varlistentry>
				<term>restart</term>
				<listitem>
					<para>
						matrix of integer (size 1 x 1) : the number of iterations before restarting the algorithm (default = min(m,n))
					</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>maxiter</term>
				<listitem>
					<para>
						matrix of integer (size 1 x 1) : the maximum number of iterations in the algorithm (default = 3000)
					</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>tol</term>
				<listitem>
					<para>
						matrix of integer (size 1 x 1) : the threshold of tolerance for stopping the algorithm (default = 1.e-6)
					</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>x</term>
				<listitem>
					<para>
						matrix of double (size n x 1) : solution vector x in the least square problem min(b-Ax)
					</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>nbiter</term>
				<listitem>
					<para>
						matrix of integer (size 1 x 1) : total number of necessary iterations to find the solution vector
					</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>res</term>
				<listitem>
					<para>
						matrix of double (size 1 x 1) : norm of the computed residual r=A'*b-A'*A*x
					</para>
				</listitem>
			</varlistentry>
		</variablelist>
	</refsection>
	<refsection>
		<title>Description</title>
		<para>
			This function solves the sparse linear least squares problem min |Ax-b|, 
			with the BA-GMRES method with Greville's preconditionning.
		</para>
		<para>
			Any optional argument equal to the empty matrix [] is 
			replaced by its default value.
		</para>
		<para>
			If the algorithm does not converge, a warning is produced.
		</para>
		<para>
			The GMRES method consist in solving the least square problem min(b-Ax), for A square matrix.
			But when A is not square, GMRES can't be directly used and need to be slightly modified.
		</para>
		<para>
			The BA-GMRES method consist in solving the least square problem min(Bb-BAx), ie finding x such as the norm of Bb-BAx is minimal.
			This method is mainly useful for overdetermined problems, ie m &gt; n for A of size m x n,
			because the matrix obtained by doing the product of B and A is of size n x n.
		</para>
		<para>
			The Greville's method is a preconditioning technique applied before an iterative method (such as GMRES or CGLS) for solving least square problems.
			This methods seeks to calculate an approximation of A^(-1).
			Indeed, it gives an upper triangular matrix Z, a diagonal matrix D and a matrix V such as A^-1 ~= Z(D^-1)V.
			If A is full column rank, then the implementation of the preconditioner B is as follow : we have C=Z(D^-1)Z and B=C(A^T).
			When A is not full column rank, a switching tolerance parameter is used to detect with a chosen accuracy the linear dependency of the columns of A.
			The accuracy and speed of the method are strongly dependant of the choice of the dropping and switching tolerance parameters.
			If m &lt; n, A^T is automatically used rather than A to calculate the preconditioner and then B^T is taken as a preconditioner in the iterative method.
		</para>
		<para>
			Restart can be used to save memory but is not adviced in this case. The initial vector x0 for the iterative process has been set up to a vector of zeros.
		</para>					
	</refsection>
	<refsection>
		<title>Examples</title>
		<programlisting role="example">
			<![CDATA[
			path = fullfile(splspc_getpath(),"tests","matrices");
			filename=fullfile(path,"lp_scfxm1.mtx");
			A = mmread(filename);
			b=ones(size(A,1),1);
			// With default settings
			x=splspc_grevgmresba(A,b);
			norm(A*x-b)/norm(b)
			// Configure the options
			tgrev=1.e-3;
			tsgrev=1.e-6;
			restart=1000;
			max_iter=3000;
			tol=1.e-6;
			[x,nbiter,res]=splspc_grevgmresba(A,b,tgrev,tsgrev,restart,max_iter,tol);
			// Configure max_iter
			x=splspc_grevgmresba(A,b,[],[],[],max_iter);
			]]>
		</programlisting>
	</refsection>
	<refsection>
		<title>Authors</title>
		<para> 1994 - 2008 - Sparselib++ v1.7 - R. Pozo, K. Remington, and A. Lumsdaine </para>
		<para> 2011 - National Institute of Informatics - K. Hayami, X. Cui and J.-F. Yin</para>
		<para> 2011 - DIGITEO - Michael Baudin </para>
		<para> 2011 - National Institute of Informatics - Benoit Goepfert  </para>
	</refsection>
	<refsection>
		<title>See Also</title>
		<simplelist type="inline">
			<member>
				<link linkend="splspc_gmresab">splspc_gmresab</link>
			</member>
			<member>
				<link linkend="splspc_gmresba">splspc_gmresba</link>
			</member>			      
			<member>
				<link linkend="splspc_rifgmresab">splspc_rifgmresab</link>
			</member>
			<member>
				<link linkend="splspc_rifgmresba">splspc_rifgmresba</link>
			</member>				
			<member>
				<link linkend="splspc_grevgmresab">splspc_grevgmresab</link>
			</member>
			<member>
				<link linkend="splspc_nrsorgmresba">splspc_nrsorgmresba</link>
			</member>			
			<member>
				<link linkend="splspc_getpath">splspc_getpath</link>
			</member>             
		</simplelist>
	</refsection>
</refentry>
