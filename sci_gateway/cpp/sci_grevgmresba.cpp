// ========================================================================
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2011 - National Institute of Informatics - Benoit Goepfert
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ========================================================================

/*C includes*/
extern "C" {
#include <stdio.h>
}

/* C++ includes */
#include <algorithm>

/*API scilab includes*/
extern "C" {
#include "stack-c.h"
#include "api_scilab.h" 
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "gw_splspc.h"
#include "gw_splspc_support.h"
}

/*local includes*/
#include "llgrevpre_double.h"
#include "gmresls.h"

using namespace std;

int sci_grevgmresba (char *fname)
{ 
	int iRowsA=0, iColsA=0, iRowsb=0, iColsb=0, iRowsx=0, iColsx=0, i;
	int maxiter=0, iNnzA=0, restart=0, result=1, sciErrNb=0;
	double tol=0.0, tgrev=0.0, tsgrev=0.0, itertime=0.0, pretime=0.0;
	int *piNbItemRowA=NULL, *piColPosA=NULL, *piAddr=NULL, *piPtrRowA=NULL;
	double *b=NULL, *xx=NULL, *pdblRealA=NULL;

	int readFlag;

	SciErr sciErr;

	CheckRhs(2,7);
	CheckLhs(1,3);

	/*get input argument 1: matrix A*/
	readFlag = splspc_getmatrix (fname, 1, &iRowsA,  &iColsA,  &iNnzA,  &piNbItemRowA, 
		&piColPosA,  &pdblRealA,  &piPtrRowA);
	if (readFlag==0)
	{
		return 0;
	}

	/*get input argument 2: vector b*/
	readFlag = splspc_getvector (fname, 2, iRowsA, &iRowsb, &iColsb, &b);
	if (readFlag==0)
	{
		return 0;
	}

	/*get input argument 3: double tgrev*/
	readFlag = splspc_getScalarDouble (fname, 3, Rhs, 1.e-3, &tgrev);
	if (readFlag==0)
	{
		return 0;
	}
	readFlag = splspc_checkDoubleInRange (fname, 3, tgrev, 0., 1.);
	if (readFlag==0)
	{
		return 0;
	}

	/*get input argument 4: double tsgrev*/
	readFlag = splspc_getScalarDouble (fname, 4, Rhs, 1.e-6, &tsgrev);
	if (readFlag==0)
	{
		return 0;
	}
	readFlag = splspc_checkDoubleInRange (fname, 4, tsgrev, 0., 1.);
	if (readFlag==0)
	{
		return 0;
	}

	/*get input argument 5: int restart*/
	readFlag = splspc_getScalarIntegerFromScalarDouble (fname, 5, Rhs, min(iRowsA,iColsA), &restart);
	if (readFlag==0)
	{
		return 0;
	}
	readFlag = splspc_checkIntegerInRange (fname, 5, restart, 0, INT_MAX);
	if (readFlag==0)
	{
		return 0;
	}

	/*get input argument 6: int maxiter*/
	readFlag = splspc_getArgumentMaxiter (fname, 6, iRowsA, iColsA, &maxiter);
	if (readFlag==0)
	{
		return 0;
	}

	/*get input argument 7: double tol*/
	readFlag = splspc_getArgumentTol (fname, 7, &tol);
	if (readFlag==0)
	{
		return 0;
	}


	CompRow_Mat_double spA(iRowsA, iColsA, iNnzA, pdblRealA, piPtrRowA, piColPosA, 0);
	CompCol_Mat_double spA2(iColsA, iRowsA, iNnzA, pdblRealA, piColPosA, piPtrRowA, 0);	
	CompCol_LLGrevPreconditioner_double *Mgrev;
	if (iRowsA<iColsA)
	{
		Mgrev = new CompCol_LLGrevPreconditioner_double(spA2, tgrev, tsgrev, pretime);
	}
	else
	{
		Mgrev = new CompCol_LLGrevPreconditioner_double(spA, tgrev, tsgrev, pretime);	
	}     
	VECTOR_double bb(b, iRowsb);
	VECTOR_double x(spA.dim(1), 0.0);
	MATRIX_double H(restart+1,restart,0.0);         

	/*MAIN COMPUTATION : GMRESBA with GREVILLE*/
	result=GMRESBA(spA, x, bb, *Mgrev, H, restart, maxiter, tol, itertime);

	/*create ouptut argument 1 : vector x*/
	iRowsx=x.size();
	iColsx=1;
	readFlag = splspc_allocDoubleVector (fname, iRowsx, &xx);
	if (readFlag==0)
	{
		return 0;
	}
	for (i=0;i<iRowsx;i++){
		xx[i]=x(i);
	}
	sciErr=createMatrixOfDouble(pvApiCtx, Rhs + 1, iRowsx, iColsx, xx);  
	if (sciErr.iErr){
		printError(&sciErr, 0);
		return 0;
	}

	if (result!=0)
	{
		splspc_warningNonconvergence(fname);
	}

	/*create ouput argument 2 : int maxiter*/
	sciErrNb=createScalarDouble(pvApiCtx, Rhs + 2, maxiter);

	/*create ouput argument 3 : double tol*/
	sciErrNb=createScalarDouble(pvApiCtx, Rhs + 3, tol);

	// Free memory
	free(xx);

	LhsVar(1) = Rhs + 1;
	if (Lhs>=2){
		LhsVar(2) = Rhs + 2;
	}
	if (Lhs>=3){
		LhsVar(3) = Rhs + 3;
	}
	return 0;
}
