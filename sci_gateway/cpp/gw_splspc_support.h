
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//                                                                           //
// This file must be used under the terms of the CeCILL.                     //
// This source file is licensed as described in the file COPYING, which      //
// you should have received as part of this distribution.  The terms         //
// are also available at                                                     //
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                  //

//
// gw_splpsc_support.h
//   Support functions for the SPLSPC gateway functions.
//
#ifndef __SCI_GW_SPLSPC_SUPPORT_H__
#define __SCI_GW_SPLSPC_SUPPORT_H__

extern "C" {

	// Functions generic of any interface
	int splspc_getmatrix (char *fname, int ivar, int * iRowsA, int * iColsA, int * iNbItemA, int ** piNbItemRowA, 
		int ** piColPosA, double ** ppdblRealA, int ** piPtrRowA);
	int splspc_getvector (char *fname, int ivar, int iRowsA, int * iRowsb, int * iColsb, double ** b);
	int splspc_getScalarIntegerFromScalarDouble (char *fname, int ivar, int rhs, int defaultint, int * myint);
	int splspc_checkIntegerInRange (char *fname, int ivar, int myint, int minint, int maxint);
	int splspc_getScalarDouble (char *fname, int ivar, int rhs, double defaultdouble, double * mydouble);
	int splspc_checkDoubleInRange (char *fname, int ivar, double mydouble, double mindouble, double maxdouble);

	// Functions specific to this module.
	int splspc_getArgumentTol (char *fname, int ivar, double * tol);
	int splspc_getArgumentMaxiter (char *fname, int ivar, int iRowsA, int iColsA, int * maxiter);
	int splspc_allocDoubleVector (char *fname, int n, double ** dblmem);
	int splspc_allocIntVector (char *fname, int n, int ** intmem);
	int splspc_warningNonconvergence(char *fname);

}
#endif /* __SCI_GW_SPLSPC_SUPPORT_H__ */
