// ========================================================================
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2011 - National Institute of Informatics - Benoit Goepfert
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ========================================================================

/*C includes*/
extern "C" {
#include <stdio.h>
}

/*C++ includes*/
#include <algorithm>

/*API scilab includes*/
extern "C" {
#include "stack-c.h"
#include "api_scilab.h" 
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "gw_splspc.h"
#include "gw_splspc_support.h"
}

/*local includes*/
#include "zrifpre_double.h"
#include "gmresls.h"

using namespace std;

int sci_gmresab (char *fname)
{ 
	int iRowsA=0, iColsA=0, iRowsb=0, iColsb=0, iRowsx=0, iColsx=0, i;
	int maxiter=0, iNnzA=0, restart=0, result=1, sciErrNb=0;
	double tol=0.0, itertime=0.0;
	int *piNbItemRowA=NULL, *piColPosA=NULL, *piAddr=NULL, *piPtrRowA=NULL;
	double *b=NULL, *xx=NULL, *pdblRealA=NULL;
	int readFlag;

	SciErr sciErr;

	CheckRhs(2,5);
	CheckLhs(1,3);

	/*get input argument 1: matrix A*/
	readFlag = splspc_getmatrix (fname, 1, &iRowsA,  &iColsA,  &iNnzA,  &piNbItemRowA, 
		&piColPosA,  &pdblRealA,  &piPtrRowA);
	if (readFlag==0)
	{
		return 0;
	}

	/*get input argument 2: vector b*/
	readFlag = splspc_getvector (fname, 2, iRowsA, &iRowsb, &iColsb, &b);
	if (readFlag==0)
	{
		return 0;
	}

	/*get input argument 3: int restart*/
	readFlag = splspc_getScalarIntegerFromScalarDouble (fname, 3, Rhs, min(iRowsA,iColsA), &restart);
	if (readFlag==0)
	{
		return 0;
	}
	readFlag = splspc_checkIntegerInRange (fname, 3, restart, 0, INT_MAX);
	if (readFlag==0)
	{
		return 0;
	}

	/*get input argument 4: int maxiter*/
	readFlag = splspc_getArgumentMaxiter (fname, 4, iRowsA, iColsA, &maxiter);
	if (readFlag==0)
	{
		return 0;
	}

	/*get input argument 5: double tol*/
	readFlag = splspc_getArgumentTol (fname, 5, &tol);
	if (readFlag==0)
	{
		return 0;
	}

	CompRow_Mat_double spA(iRowsA, iColsA, iNnzA, pdblRealA, piPtrRowA, piColPosA, 0);     
	VECTOR_double bb(b, iRowsb);
	VECTOR_double x(spA.dim(1), 0.0);
	MATRIX_double H(restart+1,restart,0.0);         

	/*MAIN COMPUTATION : GMRESAB*/
	result=GMRESAB(spA, x, bb, H, restart, maxiter, tol, itertime);

	/*create ouptut argument 1 : vector x*/
	iRowsx=x.size();
	iColsx=1;
	readFlag = splspc_allocDoubleVector (fname, iRowsx, &xx);
	if (readFlag==0)
	{
		return 0;
	}
	for (i=0;i<iRowsx;i++){
		xx[i]=x(i);
	}
	sciErr=createMatrixOfDouble(pvApiCtx, Rhs + 1, iRowsx, iColsx, xx);  
	if (sciErr.iErr){
		printError(&sciErr, 0);
		return 0;
	}

	if (result!=0)
	{
		splspc_warningNonconvergence(fname);
	}

	/*create ouput argument 2 : int maxiter*/
	sciErrNb=createScalarDouble(pvApiCtx, Rhs + 2, maxiter);

	/*create ouput argument 3 : double tol*/
	sciErrNb=createScalarDouble(pvApiCtx, Rhs + 3, tol);

	// Free memory
	free(xx);

	LhsVar(1) = Rhs + 1;
	if (Lhs>=2){
		LhsVar(2) = Rhs + 2;
	}
	if (Lhs>=3){
		LhsVar(3) = Rhs + 3;
	}
	return 0;
}
