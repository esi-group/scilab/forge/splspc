/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* Copyright (C) 2011 - DIGITEO - Michael Baudin                             */
/* Copyright (C) 2011 - National Institute of Informatics - Benoit Goepfert  */
/*                                                                           */
/* This file must be used under the terms of the CeCILL.                     */
/* This source file is licensed as described in the file COPYING, which      */
/* you should have received as part of this distribution.  The terms         */
/* are also available at                                                     */
/* http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                  */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/*C includes*/
extern "C" {
#include <stdio.h>
}

/* C++ includes */
#include <algorithm>

/*API scilab includes*/
extern "C" {
#include "stack-c.h"
#include "api_scilab.h" 
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
}

/*Gateway includes*/
extern "C" {
#include "gw_splspc_support.h"
}


using namespace std;

// Get input argument #ivar: matrix A
// Returns 0 in case of problem, returns 1 if no problem occurs.
//
// Arguments
// fname (input): the name of the function
// ivar (input): the index of the input argument
// iRowsA (output): the number of rows
// iColsA (output): the number of columns
// iNnzA (output): the number of nonzeros
// piNbItemRowA[0,1,...,iRowsA-1] (output): the number of nonzeros in each row
// piColPosA[0,1,...,iNnzA-1] (output): the column indices of the nonzeros
// pdblRealA[0,1,...,iNnzA-1] (output): the nonzero values, row-by-row
// piPtrRowA[0,1,...,iRowsA-1] (output): the starting index of each row in pdblRealA
// 
// Description
// If there is a problem, generate an error and return 0.
// If there is no problem, returns 1.
//
// A must be:
// * real (getSparseMatrix checks this)
// * sparse
// Moreover: 
// * update piColPosA: start from 1 -> start from 0
// * compute piPtrRowA: the starting index of each row (i.e. cumulated from piNbItemRowA)
//
int splspc_getmatrix (char *fname, int ivar, int * iRowsA, int * iColsA, int * iNnzA, int ** piNbItemRowA, 
	int ** piColPosA, double ** pdblRealA, int ** piPtrRowA)
{ 
	int i;
	int *piAddr=NULL;
	SciErr sciErr;

	/*get input argument ivar: matrix A*/
	sciErr = getVarAddressFromPosition(pvApiCtx, ivar, &piAddr);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}  
	if (!(isSparseType(pvApiCtx, piAddr)))
	{
		Scierror(204,_("%s: input %d must be a sparse matrix \n"),fname, ivar);
		return 0;
	}
	sciErr=getSparseMatrix(pvApiCtx, piAddr, iRowsA, iColsA, iNnzA, piNbItemRowA, piColPosA, pdblRealA);	  	    	
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	//shift of the index of each element in col_ind (start from 1 -> start from 0)
	for (i=0;i<*iNnzA;i++)
	{
		(*piColPosA)[i]-=1;
	}  
	//calculation of the array of pointers from the piNbItemRow array
	//(that only gives the number of non zeros elements for each row)
	*piPtrRowA=(int *)calloc(*iRowsA+1,sizeof(int));
	(*piPtrRowA)[0]=0;
	for (i=1;i<*iRowsA+1;i++)
	{
		(*piPtrRowA)[i]=(*piPtrRowA)[i-1]+(*piNbItemRowA)[i-1];
	}  

	return 1;
}

// get input argument #ivar: vector b
//
// Arguments
// fname (input): the name of the function
// ivar (input): the index of the input argument
// iRowsA (output): the number of rows in A
// iRowsb (output): the number of rows in b
// iColsb (output): the number of columns in b
// b[0,1,...,iRowsb-1] (output): the entries in b
//
// Description
// If there is a problem, generate an error and return 0.
// If there is no problem, returns 1.
//
//  * we check that iColsb==1
//  * we check that iRowsb==iRowsA
//  * b is guaranteed to be not complex: getMatrixOfDouble checks this.
int splspc_getvector (char *fname, int ivar, int iRowsA, int * iRowsb, int * iColsb, double ** b)
{
	int *piAddr=NULL;

	SciErr sciErr;

	sciErr=getVarAddressFromPosition(pvApiCtx, ivar, &piAddr);
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	sciErr=getVarDimension(pvApiCtx, piAddr, iRowsb, iColsb);
	if (sciErr.iErr){
		printError(&sciErr, 0);
		return 0;
	}
	if (!isDoubleType(pvApiCtx, piAddr))
	{
		Scierror(204,_("%s: input %d must be a dense matrix \n"),fname, 2);
		return 0;
	}
	if (*iColsb!=1)
	{
		Scierror(204,_("%s: input %d must be a vector \n"),fname, 2);
		return 0;
	}
	if (*iRowsb!=iRowsA)
	{
		Scierror(204,_("%s: input %d must have the same row length than input %d \n"),fname, 2, 1);
		return 0;
	}			  
	sciErr=getMatrixOfDouble(pvApiCtx, piAddr, iRowsb, iColsb, b);   	    	
	if (sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	return 1;
}

// 
// splspc_Double2IntegerArgument --
//   Convert the given double into an integer.
//
// Arguments
//   fname (input) : the name of the Scilab function generating this error
//   ivar (input) : the index of the input variable
//   dvalue (input) : the source double value
//   ivalue (output) : the target integer value
//
// Description
// If there is a problem, generate an error and return 0.
// If there is no problem, returns 1.
//
// Check that the double is in the integer range.
// Check that the double has no fractional part.
//
int splspc_Double2IntegerArgument ( char * fname , int ivar , double dvalue , int * ivalue )
{
	if ( dvalue > INT_MAX ) {
		Scierror(999,_("%s: Too large integer value in argument #%d: found %e while maximum value is %d.\n"),fname,ivar , dvalue , INT_MAX );
		return 0;
	}
	if ( dvalue < INT_MIN ) {
		Scierror(999,_("%s: Too large integer value in argument #%d: found %e while minimum value is %d.\n"),fname,ivar , dvalue , INT_MIN );
		return 0;
	}
	*ivalue = (int)dvalue;
	// Now check that the double was really an integer, with 
	// a zero fractionnal part.
	if ( (double)*ivalue != dvalue ) {
		Scierror(999,_("%s: Wrong integer in argument #%d: found %e which is different from the closest integer %d.\n"),fname,ivar , dvalue , *ivalue );
		return 0;
	}

	return 1;
}

// Get a scalar integer from a Double.
//
// Arguments
// fname (input): the name of the function
// ivar (input): the index of the input argument
// defaultint (input): the default value of the integer.
// myint (output): the required integer
//
// Description
// If there is a problem, produces an error and return 0.
// If there is no problem, returns 1.
//
// If rhs < ivar, then set the default value.
// If rhs >= ivar, and the value is [], then set the default value.
// If not, then set the actual value.
//
int splspc_getScalarIntegerFromScalarDouble (char *fname, int ivar, int rhs, int defaultint, int * myint)
{
	int *piAddr=NULL;
	int iRows, iCols;
	int readFlag=0;
	int sciErrNb=0;

	double mydouble;

	SciErr sciErr;

	if (rhs>=ivar)
	{
		sciErr=getVarAddressFromPosition(pvApiCtx, ivar, &piAddr);
		if (sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}
		sciErr=getVarDimension(pvApiCtx, piAddr, &iRows, &iCols);
		if (sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}
		if (iRows!=0)
		{			  
			if (isDoubleType(pvApiCtx, piAddr))
			{
				sciErr=getVarDimension(pvApiCtx, piAddr, &iRows, &iCols);
				if (!((iCols==1)&&(iRows==1)))
				{
					Scierror(204,_("%s: input %d must be a scalar \n"),fname, ivar);
					return 0;
				}
			}
			else
			{
				Scierror(204,_("%s: input %d must be a dense matrix of double\n"),fname, ivar);
				return 0;
			}
			sciErrNb=getScalarDouble(pvApiCtx, piAddr, &mydouble);
			readFlag = splspc_Double2IntegerArgument ( fname , ivar , mydouble , myint );
			if ( !readFlag )
			{
				return 0;
			}
		}
		else //default when argument is []
		{
			*myint=defaultint; 
		}		
	}
	else // default when no argument is given
	{
		*myint=defaultint; 
	}
	return 1;
}

// Check that an integer is in a given range
//
// Arguments
// fname (input): the name of the function
// ivar (input): the index of the input argument
// myint (input): the integer to check
// minint (input): the minimum value of the integer.
// maxint (input): the maximum value of the integer.
//
// Description
// If myint is not in range, produce an error and returns 1.
// If myint is OK, returns 0.
//
int splspc_checkIntegerInRange (char *fname, int ivar, int myint, int minint, int maxint)
{
	if ( myint > maxint )
	{
		Scierror(204,_("%s: Wrong value for input argument #%d: Must be < %d.\n"),fname, ivar, maxint);
		return 0;
	}
	if ( myint < minint )
	{
		Scierror(204,_("%s: Wrong value for input argument #%d: Must be > %d.\n"),fname, ivar, minint);
		return 0;
	}
	return 1;
}

// Get a scalar double.
//
// Arguments
// fname (input): the name of the function
// ivar (input): the index of the input argument
// defaultdouble (input): the default value of the double.
// mydouble (output): the required double
//
// Description
// If there is a problem, produces an error and return 0.
// If there is no problem, returns 1.
//
// If rhs < ivar, then set the default value.
// If rhs >= ivar, and the value is [], then set the default value.
// If not, then set the actual value.
//
int splspc_getScalarDouble (char *fname, int ivar, int rhs, double defaultdouble, double * mydouble)
{
	int *piAddr=NULL;
	int iRows, iCols;
	int readFlag=0;
	int sciErrNb=0;

	SciErr sciErr;

	if (rhs>=ivar)
	{
		sciErr=getVarAddressFromPosition(pvApiCtx, ivar, &piAddr);
		if (sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}
		sciErr=getVarDimension(pvApiCtx, piAddr, &iRows, &iCols); 
		if (sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}
		if (iRows!=0)
		{			  
			if (isDoubleType(pvApiCtx, piAddr))
			{
				if (!((iCols==1)&&(iRows==1)))
				{
					Scierror(204,_("%s: input %d must be a scalar \n"),fname, ivar);
					return 0;
				}
			}
			else
			{
				Scierror(204,_("%s: input %d must be a dense matrix of double \n"),fname, ivar);
				return 0;
			}
			sciErrNb=getScalarDouble(pvApiCtx, piAddr, mydouble);
		}
		else //default when argument is []
		{
			*mydouble=defaultdouble; 
		}  		   	    	
	}
	else // default when no argument is given
	{
		*mydouble=defaultdouble; 
	} 
	return 1;
}

// Check that a double is in a given range
//
// Arguments
// fname (input): the name of the function
// ivar (input): the index of the input argument
// mydouble (input): the double to check
// mindouble (input): the minimum value of the double.
// maxdouble (input): the maximum value of the double.
//
// Description
// If myint is not in range, produce an error and returns 0.
// If myint is OK, returns 1.
//
int splspc_checkDoubleInRange (char *fname, int ivar, double mydouble, double mindouble, double maxdouble)
{
	if ( mydouble > maxdouble )
	{
		Scierror(204,_("%s: Wrong value for input argument #%d: Must be < %f.\n"),fname, ivar, maxdouble);
		return 0;
	}
	if ( mydouble < mindouble )
	{
		Scierror(204,_("%s: Wrong value for input argument #%d: Must be > %f.\n"),fname, ivar, mindouble);
		return 0;
	}
	return 1;
}

// Get the tolerance tol.
//
// Arguments
// fname (input): the name of the function
// ivar (input): the index of the input argument
// tol (output): the tolerance
//
// Description
// If tol is not correct, produce an error and returns 0.
// If tol is OK, returns 1.
//
// The default value of tol is 1.e-6.
//
int splspc_getArgumentTol (char *fname, int ivar, double * tol)
{
	int readFlag;

	readFlag = splspc_getScalarDouble (fname, ivar, Rhs, 1.e-6, tol);
	if (readFlag==0)
	{
		return 0;
	}
	readFlag = splspc_checkDoubleInRange (fname, ivar, *tol, 0., 1.);
	if (readFlag==0)
	{
		return 0;
	}
	return 1;
}

// Get the maximum number of iterations maxiter.
//
// Arguments
// fname (input): the name of the function
// ivar (input): the index of the input argument
// iRowsA (input): the number of rows in A
// iColsA (input): the number of columns in A
// maxiter (output): the maximum number of iterations
//
// Description
// If tol is not correct, produce an error and returns 0.
// If tol is OK, returns 1.
//
// The default value of maxiter is min(iRowsA,iColsA).
//
int splspc_getArgumentMaxiter (char *fname, int ivar, int iRowsA, int iColsA, int * maxiter)
{
	int readFlag;

	readFlag = splspc_getScalarIntegerFromScalarDouble (fname, ivar, Rhs, min(iRowsA,iColsA), maxiter);
	if (readFlag==0)
	{
		return 0;
	}
	readFlag = splspc_checkIntegerInRange (fname, ivar, *maxiter, 0, INT_MAX);
	if (readFlag==0)
	{
		return 0;
	}
	return 1;
}

// Allocate an array of doubles
//
// Description
// If allocation is not correct, produce an error and returns 0.
// If allocation is OK, returns 1.
//
int splspc_allocDoubleVector (char *fname, int n, double ** dblmem)
{
	*dblmem = NULL;

	*dblmem = (double *)malloc(n*sizeof(double));
	if (*dblmem==NULL)
	{
		Scierror(112, "%s: No more memory.\n",fname);
		return 0;
	}
	return 1;
}
// Allocate an array of ints
//
// Description
// If allocation is not correct, produce an error and returns 0.
// If allocation is OK, returns 1.
//
int splspc_allocIntVector (char *fname, int n, int ** intmem)
{
	*intmem = NULL;

	*intmem = (int *)malloc(n*sizeof(int));
	if (*intmem==NULL)
	{
		Scierror(112, "%s: No more memory.\n",fname);
		return 0;
	}
	return 1;
}

// Allocate an array of ints
//
// Description
// If allocation is not correct, produce an error and returns 0.
// If allocation is OK, returns 1.
//

int splspc_warningNonconvergence(char *fname)
{
	sciprint(_("%s: Warning :\n"),fname);
	sciprint(_("Algorithm did not converge.\n"));
	return 0;
}
