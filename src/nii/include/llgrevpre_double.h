/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* Copyright (C) 2009 - National Institute of Informatics - Xiaoke Cui       */
/* Copyright (C) 2011 - DIGITEO - Michael Baudin                             */
/* Copyright (C) 2011 - National Institute of Informatics - Benoit Goepfert  */
/*                                                                           */
/* This file must be used under the terms of the CeCILL.                     */
/* This source file is licensed as described in the file COPYING, which      */
/* you should have received as part of this distribution.  The terms         */
/* are also available at                                                     */
/* http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                  */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*                                                                           */
/*                     A Package based on                                    */
/*                     SparseLib++ : Sparse Matrix Library                   */
/*                                                                           */
/*               Institute of Computational Mathematics and                  */
/*                      Scientific/Engineering Computing                     */
/*                       Authors:  Xiaoke Cui                                */
/*                                                                           */
/*                                 NOTICE                                    */
/*                                                                           */
/* Permission to use, copy, modify, and distribute this software and         */
/* its documentation for any purpose and without fee is hereby granted       */
/* provided that the above notice appear in all copies and supporting        */
/* documentation.                                                            */
/*                                                                           */
/* Neither the Institutions (Institute of Computational Mathematics and      */
/* Scientific/Engineering Computing, Chinese Academy of Sciences) nor the    */
/* Authors make any representations about the suitability of this software   */
/* for any purpose.  This software is provided ``as is'' without expressed   */
/* or implied warranty.                                                      */
/*                                                                           */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
#ifndef LLGREVPRE_H
#define LLGREVPRE_H

#ifdef _MSC_VER
	#if LIBSPLSPC_EXPORTS 
		#define SPLSPC_IMPORTEXPORT __declspec (dllexport)
	#else
		#define SPLSPC_IMPORTEXPORT __declspec (dllimport)
	#endif
#else
	#define SPLSPC_IMPORTEXPORT
#endif

#include "vecdefs.h"
#include VECTOR_H
#include MATRIX_H
#include "compcol_double.h"
#include "comprow_double.h"

class SPLSPC_IMPORTEXPORT CompCol_LLGrevPreconditioner_double{

 private:
	VECTOR_double d_val_;
	VECTOR_double Z_val_;
	VECTOR_int    Z_colptr_;
	VECTOR_int    Z_rowind_;
	int Z_nz_;
	VECTOR_double V_val_;
	VECTOR_int    V_colptr_;
	VECTOR_int    V_rowind_;
	int V_nz_;
	int dim_[2];
  
 public:
	CompCol_LLGrevPreconditioner_double(void);
	CompCol_LLGrevPreconditioner_double(const CompCol_Mat_double &A, const double tau, const double taus, double &pretime);  
	~CompCol_LLGrevPreconditioner_double(void){};

	VECTOR_double     Bsolve(const CompCol_Mat_double &A, const VECTOR_double &x) const; 
	VECTOR_double     solve(const VECTOR_double &x) const;  
	VECTOR_double     trans_solve(const VECTOR_double &x) const;
	
	//getters
	double *get_D_val(void) const;
	double *get_Z_val(void) const;
	int *get_Z_colptr(void) const;
	int *get_Z_rowind(void) const;
	int get_Z_nz(void) const;
	double *get_V_val(void) const;
	int *get_V_colptr(void) const;
	int *get_V_rowind(void) const;
	int get_V_nz(void) const;	
	const int *get_dims(void) const;
	
};

#endif
