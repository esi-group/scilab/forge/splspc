/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* Copyright (C) 2007 - National Institute of Informatics - Jun-Feng Yin     */
/* Copyright (C) 2011 - DIGITEO - Michael Baudin                             */
/* Copyright (C) 2011 - National Institute of Informatics - Benoit Goepfert  */
/*                                                                           */
/* This file must be used under the terms of the CeCILL.                     */
/* This source file is licensed as described in the file COPYING, which      */
/* you should have received as part of this distribution.  The terms         */
/* are also available at                                                     */
/* http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                  */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#ifndef GMRESLS_H
#define GMRESLS_H

#ifdef _MSC_VER
	#if LIBSPLSPC_EXPORTS 
		#define SPLSPC_IMPORTEXPORT __declspec (dllexport)
	#else
		#define SPLSPC_IMPORTEXPORT __declspec (dllimport)
	#endif
#else
	#define SPLSPC_IMPORTEXPORT
#endif

//C includes
extern "C"{
	#include <stdio.h>
	#include <sciprint.h>
	#include <time.h>
}

template < class Matrix, class Vector >
SPLSPC_IMPORTEXPORT void 
Update(Vector &x, int k, Matrix &h, Vector &s, Vector v[])
{
	Vector y(s);
	x = 0.0;
	// Backsolve:  
	for (int i = k; i >= 0; i--) 
	{
		y(i) /= h(i,i);
	    for (int j = i - 1; j >= 0; j--)
	    {
			y(j) -= h(j,i) * y(i);
		}
	}

	for (int j = 0; j <= k; j++)
	{
		x += v[j] * y(j);
	}
}


template < class Real >
SPLSPC_IMPORTEXPORT Real abs(Real x)
{
	return (x > 0 ? x : -x);
}

/***********************************/
/**BA-GMRES without preconditioner**/
/***********************************/
template < class Operator, class Vector,
           class Matrix, class Real >
SPLSPC_IMPORTEXPORT int GMRESBA(const Operator &A, Vector &x, const Vector &b,
								Matrix &H, int &m, int &max_iter,
								Real &tol, Real &itertime)
{	
	Real resid;
	int i, j = 1, k;
	Vector s(m+1), cs(m+1), sn(m+1), w;  
	Real last = clock();
	
	/*initialization*/
	Vector r = A.trans_mult(b - A * x);
	Real normr = norm(r);
	Real normATb = norm(A.trans_mult(b));
	Real normATr = normr;
	
	/*preliminary tests*/	
	if (normATb == 0.0)
	{
		normATb = 1.0;
	}	
	resid = normATr / normATb;
	if (resid < tol)
	{
		tol = resid;
		max_iter = 0;
		itertime = (clock()-last)/CLOCKS_PER_SEC;
		//sciprint("runs %d iterations, cost %lf sec.\n||A^T * r_0||/||A^T * b|| = %lf\n", max_iter, itertime, tol);
		//sciprint("exit after the initialization because of a too small residual\n");
		return 1;
	}		
	Vector *v = new Vector[m+1];
	
	/*restarted GMRES*/
	while (j <= max_iter)
	{ 
		v[0] = r * (1.0 / normr);  
		s = 0.0;
		s(0) = normr;
		
		/*GMRES*/
		for (i = 0; i < m && j <= max_iter; i++, j++) 
		{	
			/*Arnoldi's process*/	
			w = A.trans_mult(A * v[i]);
			for (k = 0; k <= i; k++)
			{
				H(k, i) = dot(w, v[k]);
				w -= H(k, i) * v[k];
			}
			H(i+1, i) = norm(w);
			
			/*breakdown*/
			if ( fabs(H(i+1,i))<1.e-15)
			{	
				tol = resid;
				max_iter = j;
				itertime = (clock()-last)/CLOCKS_PER_SEC;				
				//sciprint("runs %d iterations, cost %lf sec.\n||A^T * r_k||/||A^T * b|| = %lf\n", max_iter, itertime, tol);
				//sciprint("exit because break down happens\n");
				delete [] v;
				return 1;
			}	
			v[i+1] = w * (1.0 / H(i+1, i));
			
			/*Givens rotations*/
			for (k = 0; k < i; k++)
			{
				ApplyPlaneRotation(H(k,i), H(k+1,i), cs(k), sn(k));
			}
			GeneratePlaneRotation(H(i,i), H(i+1,i), cs(i), sn(i));
			ApplyPlaneRotation(H(i,i), H(i+1,i), cs(i), sn(i));
			ApplyPlaneRotation(s(i), s(i+1), cs(i), sn(i));
						
			//Update(x, i, H, s, v); //case 2
			//normATr = norm(A.trans_mult(b-A*x)); //case 2
			resid = abs(s(i+1))/normATb; //case 1
			//resid = normATr/normATb; //case 2
								
			/* stopping criterion */
			if (resid < tol){
				Update(x, i, H, s, v); //case 1
				tol = resid;
				max_iter = j;
				itertime = (clock()-last)/CLOCKS_PER_SEC;
				//sciprint("runs %d iterations, cost %lf sec.\n||A^T * r_k||/||A^T * b|| = %lf\n", max_iter, itertime, tol);
				//sciprint("converge within given parameters for this problem\n");
				delete [] v;
				return 0;
			}			
		}			
		//sciprint("do not converge at this round.\n||A^T * r_k||/||A^T * b|| = %lf\n", resid);
		
		/*reinitialization*/
		Update(x, i<j ? i:j - 1, H, s, v);
		r = A.trans_mult(b - A * x);
		normr = norm(r);
		normATr = normr;
		resid =  normATr / normATb;
		if (resid < tol) 
		{
			tol = resid;
			max_iter = j;
			itertime = (clock()-last)/CLOCKS_PER_SEC;
			//sciprint("runs %d iterations, cost %lf sec.\n||A^T * r_k||/||A^T * b|| = %lf\n", max_iter, itertime, tol);
			//sciprint("converge within given parameters for this problem\n");
			delete [] v;
			return 0;	
		}
	}
	
	/*no convergence*/
	tol = resid;
	max_iter = j;	
	itertime = (clock()-last)/CLOCKS_PER_SEC;	
	//sciprint("runs %d iterations, cost %lf sec.\n||A^T * r_k||/||A^T * b|| = %lf\n", max_iter, itertime, tol);
	//sciprint("do not converge within given parameters for this problem\n");
	delete [] v;
	return 1;
}

/********************************/
/**BA-GMRES with preconditioner**/
/********************************/
template < class Operator, class Vector, class Preconditioner,
           class Matrix, class Real >
SPLSPC_IMPORTEXPORT int GMRESBA(const Operator &A, Vector &x, const Vector &b,
								const Preconditioner &B, Matrix &H, int &m, int &max_iter,
								Real &tol, double &itertime)
{
	Real resid;
	int i, j = 1, k;
	Vector s(m+1), cs(m+1), sn(m+1), w;
	Real last = clock();
	
	/*initialization*/
	Vector r = B.Bsolve(A, b - A * x);
	Real normr = norm(r);
	Real normBb = norm(B.Bsolve(A,b)); //case 1
	//Real normATr = norm(A.trans_mult(b - A*x)); //case 2	
	//Real normATb  = norm(A.trans_mult(b)); //case 2
	
	/*preliminary tests*/
	if (normBb == 0.0) //case 1
	{
		normBb = 1.0;
	}			
	//if (normATb == 0.0) //case 2
	//{
	//	normATb = 1.0;
	//}
	resid = normr /normBb; //case 1	
	//resid = normATr / normATb; //case 2
	if (resid < tol)
	{
		tol = resid;
		max_iter = 0;
		itertime = (clock()-last)/CLOCKS_PER_SEC;
		//sciprint("runs %d iterations, cost %lf sec.\n||B * r_0||/||B * b|| = %lf\n", max_iter, itertime, tol); //case 1
		//sciprint("runs %d iterations, cost %lf sec.\n||A^T * r_0||/||A^T * b|| = %lf\n", max_iter, itertime, tol); //case 2
		//sciprint("exit after the initialization because of a too small residual\n");
		return 0;
	}	
	Vector *v = new Vector[m+1];
	
	/*restarted GMRES*/
	while (j <= max_iter)
	{
		v[0] = r * (1.0 / normr);
		s = 0.0;
		s(0) = normr;
		
		/*GMRES*/
		for (i = 0; i < m && j <= max_iter; i++, j++)
		{
			/*Arnoldi's process*/
			w = B.Bsolve(A, A * v[i]);			
		    for (k = 0; k <= i; k++)
		    {
				H(k, i) = dot(w, v[k]);
				w -= H(k, i) * v[k];
			}
			H(i+1, i) = norm(w);
			
			/*breakdown*/
			if ( fabs(H(i+1,i))<1.e-15)
			{	
				tol = resid;
				max_iter = j;
				itertime = (clock()-last)/CLOCKS_PER_SEC;				
				//sciprint("runs %d iterations, cost %lf sec.\n||B * r_k||/||B * b|| = %lf\n", max_iter, itertime, tol); //case 1
				//sciprint("runs %d iterations, cost %lf sec.\n||A^T * r_k||/||A^T * b|| = %lf\n", max_iter, itertime, tol); //case 2
				//sciprint("exit because break down happens\n");
				delete [] v;
				return 1;
			}
			v[i+1] = w * (1.0 / H(i+1, i));
			
			/*Givens rotations*/		
			for (k = 0; k < i; k++)
			{
				ApplyPlaneRotation(H(k,i), H(k+1,i), cs(k), sn(k));
			}
			GeneratePlaneRotation(H(i,i), H(i+1,i), cs(i), sn(i));
			ApplyPlaneRotation(H(i,i), H(i+1,i), cs(i), sn(i));
			ApplyPlaneRotation(s(i), s(i+1), cs(i), sn(i));

			//Update(x, i, H, s, v); //case 2
			//normATr = norm(A.trans_mult(b-A*x)); //case 2
			resid = abs(s(i+1))/normBb; //case 1
			//resid = normATr/normATb; //case 2	

			/*stopping criterion*/
			if (resid < tol){
				Update(x, i, H, s, v); //case 1
				tol = resid;
				max_iter = j;
				itertime = (clock()-last)/CLOCKS_PER_SEC;			
				//sciprint("runs %d iterations, cost %lf sec.\n||B * r_k||/||B * b|| = %lf\n", max_iter, itertime, tol); //case 1
				//sciprint("runs %d iterations, cost %lf sec.\n||A^T * r_k||/||A^T * b|| = %lf\n", max_iter, itertime, tol); //case 2
				//sciprint("converge within given parameters for this problem\n");
				delete [] v;
				return 0;
			}
		}	
		//sciprint("do not converge at this round.\n||B * r_k||/||B * b|| = %lf\n", resid); //case 1	
		//sciprint("did not converge at this round.\n||A^T * r_k||/||A^T * b|| = %lf\n", resid); //case 2
		
		/*reinitialization*/
		Update(x, i<j ? i:j - 1, H, s, v);
		r = B.Bsolve(A, b-A*x);
		normr = norm(r);
		//normATr = norm(A.trans_mult(b-A*x)); //case 2		
		resid = normr / normBb; //case 1
		//resid = normATr / normATb; //case 2
		
		/* stopping criterion */
		if (resid < tol) 
		{
			tol = resid;
			max_iter = j;
			itertime = (clock()-last)/CLOCKS_PER_SEC;
			//sciprint("runs %d iterations, cost %lf sec.\n||B * r_0||/||B * b|| = %lf\n", max_iter, itertime, tol); //case 1
			//sciprint("runs %d iterations, cost %lf sec.\n||A^T * r_0||/||A^T * b|| = %lf\n", max_iter, itertime, tol); //case 2
			//sciprint("converge within given parameters for this problem\n");
			delete [] v;			
			return 0;
		}			
	}
	
	/*no convergence*/
	tol = resid;
	max_iter = j;	
	itertime = (clock()-last)/CLOCKS_PER_SEC;	
	//sciprint("runs %d iterations, cost %lf sec.\n||B * r_k||/||B * b|| = %lf\n", max_iter, itertime, tol); //case 1
	//sciprint("runs %d iterations, cost %lf sec.\n||A^T * r_k||/||A^T * b|| = %lf\n", max_iter, itertime, tol); //case 2
	//sciprint("do not converge within given parameters for this problem\n");
	delete [] v;	
	return 1;
}

/***********************************/
/**AB-GMRES without preconditioner**/
/***********************************/
template < class Operator, class Vector, class Matrix, class Real >
SPLSPC_IMPORTEXPORT int GMRESAB(const Operator &A, Vector &x, const Vector &b,
								Matrix &H, int &m, int &max_iter, Real &tol, Real &itertime)
{
	Real resid;
	int i, j = 1, k;
	Vector s(m+1), cs(m+1), sn(m+1), w;
	Real last = clock();
	
	/*initialization*/	
	Vector r = b - A * x;
	Real normr = norm(r); //case 1
	Real normb = norm(b); //case 1
	//Real normATr = A.trans_mult(r); //case 2
	//Real normATb = A.trans_mult(b); //case 2	
	
	/*preliminary tests*/
	if (normb == 0.0) //case 1
	{
		normb = 1;
	}	
	//if (normATb == 0.0) //case 2
	//{
	//	normATb = 1;
	//}	
	resid = normr/normb; //case 1
	//resid = normATr/normATb //case 2
	if (resid < tol)
	{
		tol = resid;
		max_iter = 0;
		itertime = (clock()-last)/CLOCKS_PER_SEC;
		//sciprint("runs %d iterations, cost %lf sec.\n||r_0||/||b|| = %lf\n", max_iter, itertime, tol); //case 1
		//sciprint("runs %d iterations, cost %lf sec.\n||A^T * r_0||/||A^T * b|| = %lf\n", max_iter, itertime, tol); //case 2
		//sciprint("exit after the initialization because of a too small residual\n");
		return 0;
	}	
	Vector *v = new Vector[m+1];
	Vector z(r.size(),0.0),tempz(r.size(),0.0);
	
	/*restarted GMRES*/
	while (j <= max_iter)
	{
		v[0] = r * (1.0 / normr);
		s = 0.0;
		s(0) = normr;
		
		/*GMRES*/
		for (i = 0; i < m && j <= max_iter; i++, j++)
		{
			w = A*(A.trans_mult(v[i]));
			for (k = 0; k <= i; k++)
			{
				H(k, i) = dot(w, v[k]);
				w -= H(k, i) * v[k];
			}
			H(i+1, i) = norm(w);
			
			/*breakdown*/
			if ( fabs(H(i+1,i))<1.e-15)
			{	
				tol = resid;
				max_iter = j;
				itertime = (clock()-last)/CLOCKS_PER_SEC;				
				//sciprint("runs %d iterations, cost %lf sec.\n||r_k||/||b|| = %lf\n", max_iter, itertime, tol); //case 1
				//sciprint("runs %d iterations, cost %lf sec.\n||A^T * r_k||/||A^T * b|| = %lf\n", max_iter, itertime, tol); //case 2
				//sciprint("exit because break down happens\n");
				delete [] v;
				return 1;
			}
			v[i+1] = w * (1.0 / H(i+1, i));
			
			/*Givens rotations*/
			for (k = 0; k < i; k++)
			{
				ApplyPlaneRotation(H(k,i), H(k+1,i), cs(k), sn(k));
			}
			GeneratePlaneRotation(H(i,i), H(i+1,i), cs(i), sn(i));
			ApplyPlaneRotation(H(i,i), H(i+1,i), cs(i), sn(i));
			ApplyPlaneRotation(s(i), s(i+1), cs(i), sn(i));
			
			tempz = z;
			Update(tempz, i, H, s, v);
			r = b - A * (x + A.trans_mult(tempz));
			normr = norm(r); //case 1
			//normATr = norm(A.trans_mult(r)); //case 2 		
			resid = normr/normb; //case 1
			//resid = normATr/normATb; //case 2
			
			/*stopping criterion*/														
			if ( resid < tol)
			{
				x+=A.trans_mult(tempz);
				tol = resid;
				max_iter = j;
				itertime = (clock()-last)/CLOCKS_PER_SEC;						
				//sciprint("runs %d iterations, cost %lf sec.\n||r_k||/||b|| = %lf\n", max_iter, itertime, tol); //case 1
				//sciprint("runs %d iterations, cost %lf sec.\n||A^T * r_k||/||A^T * b|| = %lf\n", max_iter, itertime, tol); //case 2
				//sciprint("converge within given parameters for this problem\n");
				delete [] v;
				return 0;
			}	
		}
		
		//sciprint("do not converge at this round.\n||r_k||/||b|| = %lf\n", resid); //case 1	
		//sciprint("did not converge at this round.\n||A^T * r_k||/||A^T * b|| = %lf\n", resid); //case 2
		
		/*reinitialization*/
		Update(z, i<j ? i:j - 1, H, s, v);		
		r = b - A * (x + A.trans_mult(z));
		normr = norm(r); //case 1
		//normATr = norm(A.trans_mult(r)); //case 2 
		resid = normr/normb; //case 1
		//resid = normATr / normATb; //case 2
		
		/* stopping criterion */
		if (resid < tol) 
		{
			x += A.trans_mult(tempz);
			tol = resid;
			max_iter = j;
			itertime = (clock()-last)/CLOCKS_PER_SEC;
			//sciprint("runs %d iterations, cost %lf sec.\n||r_0||/||b|| = %lf\n", max_iter, itertime, tol); //case 1
			//sciprint("runs %d iterations, cost %lf sec.\n||A^T * r_0||/||A^T * b|| = %lf\n", max_iter, itertime, tol); //case 2
			//sciprint("converge within given parameters for this problem\n");
			delete [] v;			
			return 0;
		}	
	}
	
	/*no convergence*/
	tol = resid;
	max_iter = j;	
	itertime = (clock()-last)/CLOCKS_PER_SEC;	
	//sciprint("runs %d iterations, cost %lf sec.\n||r_k||/||b|| = %lf\n", max_iter, itertime, tol); //case 1
	//sciprint("runs %d iterations, cost %lf sec.\n||A^T * r_k||/||A^T * b|| = %lf\n", max_iter, itertime, tol); //case 2
	//sciprint("do not converge within given parameters for this problem\n");
	delete [] v;	
	return 1;
}

/********************************/
/**AB-GMRES with preconditioner**/
/********************************/
template < class Operator, class Vector, class Preconditioner,
           class Matrix, class Real >
SPLSPC_IMPORTEXPORT int GMRESAB(const Operator &A, Vector &x, const Vector &b,
								const Preconditioner &B, Matrix &H, int &m, int &max_iter,
								Real &tol, Real &itertime)
{
	Real resid;
	int i, j = 1, k;
	Vector s(m+1), cs(m+1), sn(m+1), w;	
	Real last = clock();

	/*initialization*/
	Vector r = b - A * x;
	Real normr = norm(r); //case 1
	Real normb = norm(b); //case 1
	//Real normATr = A.trans_mult(r); //case 2
	//Real normATb = A.trans_mult(b); //case 2
	
	/*preliminary tests*/
	if (normb == 0.0) //case 1
	{
		normb = 1;
	}
	//if (normATb == 0.0) //case 2
	//{
	//	normATb = 1;
	//}	
	resid = normr/normb; //case 1
	//resid = normATr/normATb //case 2	
	if (resid < tol)
	{
		tol = resid;
		max_iter = 0;
		itertime = 0.0;
		//sciprint("runs %d iterations, cost %lf sec.\n||r_0||/||b|| = %lf\n", max_iter, itertime, tol); //case 1
		//sciprint("runs %d iterations, cost %lf sec.\n||A^T * r_0||/||A^T * b|| = %lf\n", max_iter, itertime, tol); //case 2
		//sciprint("exit after the initialization because of a too small residual\n");
		return 0;
	}	
	Vector *v = new Vector[m+1];
	Vector z(r.size(),0.0),tempz(r.size(),0.0);	
	
	/*restarted GMRES*/
	while (j <= max_iter)
	{
		v[0] = r * (1.0 / normr);
		s = 0.0;
		s(0) = normr;
		
		/*GMRES*/
		for (i = 0; i < m && j <= max_iter; i++, j++)
		{
			/*Arnoldi's process*/
			w = A *(B.Bsolve(A, v[i]));
			for (k = 0; k <= i; k++)
			{
				H(k, i) = dot(w, v[k]);
				w -= H(k, i) * v[k];
			}
			H(i+1, i) = norm(w);
			
			/*breakdown*/
			if ( fabs(H(i+1,i))<1.e-15)
			{	
				tol = resid;
				max_iter = j;
				itertime = (clock()-last)/CLOCKS_PER_SEC;				
				//sciprint("runs %d iterations, cost %lf sec.\n||r_k||/||b|| = %lf\n", max_iter, itertime, tol); //case 1
				//sciprint("runs %d iterations, cost %lf sec.\n||A^T * r_k||/||A^T * b|| = %lf\n", max_iter, itertime, tol); //case 2
				//sciprint("exit because break down happens\n");
				delete [] v;
				return 1;
			}			
			v[i+1] = w * (1.0 / H(i+1, i));
			
			/*Givens rotations*/
			for (k = 0; k < i; k++)
			{
				ApplyPlaneRotation(H(k,i), H(k+1,i), cs(k), sn(k));
			}
			GeneratePlaneRotation(H(i,i), H(i+1,i), cs(i), sn(i));
			ApplyPlaneRotation(H(i,i), H(i+1,i), cs(i), sn(i));
			ApplyPlaneRotation(s(i), s(i+1), cs(i), sn(i));
			
			tempz=z;
			Update(tempz, i, H, s, v);
			r = b - A * (x + B.Bsolve(A, tempz));			
			normr = norm(r); //case 1
			//normATr = norm(A.trans_mult(r)); //case 2 		
			resid = normr/normb; //case 1
			//resid = normATr/normATb; //case 2
			
			/* stopping criterion */			
			if ( resid  < tol)
			{
				x += B.Bsolve(A, tempz);
				tol = resid;
				max_iter = j;
				itertime = (clock()-last)/CLOCKS_PER_SEC;
				//sciprint("runs %d iterations, cost %lf sec.\n||r_k||/||b|| = %lf\n", max_iter, itertime, tol); //case 1
				//sciprint("runs %d iterations, cost %lf sec.\n||A^T * r_k||/||A^T * b|| = %lf\n", max_iter, itertime, tol); //case 2
				//sciprint("converge within given parameters for this problem\n");
				delete [] v;
				return 0;
			}
		}
		
		//sciprint("do not converge at this round.\n||r_k||/||b|| = %lf\n", resid); //case 1	
		//sciprint("did not converge at this round.\n||A^T * r_k||/||A^T * b|| = %lf\n", resid); //case 2
		
		/*reinitialization*/
		Update(z, i<j ? i:j - 1, H, s, v);		
		r = b - A * (x + B.Bsolve(A, z));
		normr = norm(r); //case 1
		//normATr = norm(A.trans_mult(r)); //case 2 
		resid = normr/normb; //case 1
		//resid = normATr / normATb; //case 2
		
		/*stopping criterion*/
		if (resid < tol) {
			x += B.Bsolve(A, tempz);
			tol = resid;
			max_iter = j;
			itertime = (clock()-last)/CLOCKS_PER_SEC;
			//sciprint("runs %d iterations, cost %lf sec.\n||r_0||/||b|| = %lf\n", max_iter, itertime, tol); //case 1
			//sciprint("runs %d iterations, cost %lf sec.\n||A^T * r_0||/||A^T * b|| = %lf\n", max_iter, itertime, tol); //case 2
			//sciprint("converge within given parameters for this problem\n");
			delete [] v;
			return 0;
		}
	}
	
	/*no convergence*/
	tol = resid;
	max_iter = j;	
	itertime = (clock()-last)/CLOCKS_PER_SEC;		
	//sciprint("runs %d iterations, cost %lf sec.\n||r_k||/||b|| = %lf\n", max_iter, itertime, tol); //case 1
	//sciprint("runs %d iterations, cost %lf sec.\n||A^T * r_k||/||A^T * b|| = %lf\n", max_iter, itertime, tol); //case 2
	//sciprint("do not converge within given parameters for this problem\n");	
	delete [] v;	
	return 1;	
}

#include <math.h> 

template<class Real> 
SPLSPC_IMPORTEXPORT void GeneratePlaneRotation(Real &dx, Real &dy, Real &cs, Real &sn)
{
	if (dy == 0.0)
	{
		cs = 1.0;
	    sn = 0.0;
	} 
	else if (abs(dy) > abs(dx)) 
	{
		Real temp = dx / dy;
	    sn = 1.0 / sqrt( 1.0 + temp*temp );
		cs = temp * sn;
	}
	else 
	{
		Real temp = dy / dx;
	    cs = 1.0 / sqrt( 1.0 + temp*temp );
		sn = temp * cs;
	}
}


template<class Real> 
SPLSPC_IMPORTEXPORT void ApplyPlaneRotation(Real &dx, Real &dy, Real &cs, Real &sn)
{
	Real temp  =  cs * dx + sn * dy;
	dy = -sn * dx + cs * dy;
	dx = temp;
}

#endif

