//===========================================================================//
// Copyright (C) 2011 - DIGITEO - Michael Baudin                             //
// Copyright (C) 2011 - National Institute of Informatics - Benoit Goepfert  //
//                                                                           //
// This file must be used under the terms of the CeCILL.                     //
// This source file is licensed as described in the file COPYING, which      //
// you should have received as part of this distribution.  The terms         //
// are also available at                                                     //
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                  //
//===========================================================================//


#ifndef _NRSOR_H_
#define _NRSOR_H_

#ifdef _MSC_VER
	#if LIBSPLSPC_EXPORTS 
		#define SPLSPC_IMPORTEXPORT __declspec (dllexport)
	#else
		#define SPLSPC_IMPORTEXPORT __declspec (dllimport)
	#endif
#else
	#define SPLSPC_IMPORTEXPORT
#endif

#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

__BEGIN_DECLS

SPLSPC_IMPORTEXPORT void F2C(bagmres)(int * m, int * n, int * nnz, 
    double * valAC, int * rowposA, int * colptrA, 
    double * valAR, int * colPosA, int * rowPtrA, 
	double * b, int * nbin, int * maxout, double * eps, double * omg, 
	double *x, double *res, int * nbout, int * conv, double * t_in, double * t_out, 
	double *H, double *V, double *Aei, double *c, double *g, 
	double *r, double *s, double *w, double *y);

__END_DECLS

#endif /* _NRSOR_H_ */

