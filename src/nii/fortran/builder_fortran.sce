// ====================================================================
// Copyright (C) 2011 - Benoit Goepfert
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================


fortran_dir   = get_absolute_file_path("builder_fortran.sce");
fortran_path  = "fortran";
linknames = ["niifortran"];
files = [
	  "bagmres.f"
  ];
ldflags = "";

if MSDOS then
  cflags = "-DWIN32 -DLIBSPLSPC_EXPORTS";
  libs     =[];			  
else
  include1 = fortran_dir;
  cflags = "-I"""+include1+"""";
  libs = [];		  
end

tbx_build_src(linknames, files, fortran_path, fortran_dir, libs, ldflags, cflags);

clear fortran_dir fortran_path linknames files ldflags tbx_build_src;
