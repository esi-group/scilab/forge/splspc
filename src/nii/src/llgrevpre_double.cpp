/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* Copyright (C) 2009 - National Institute of Informatics - Xiaoke Cui       */
/* Copyright (C) 2011 - DIGITEO - Michael Baudin                             */
/* Copyright (C) 2011 - National Institute of Informatics - Benoit Goepfert  */
/*                                                                           */
/* This file must be used under the terms of the CeCILL.                     */
/* This source file is licensed as described in the file COPYING, which      */
/* you should have received as part of this distribution.  The terms         */
/* are also available at                                                     */
/* http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                  */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*                                                                           */
/*                     A Package based on                                    */
/*                     SparseLib++ : Sparse Matrix Library                   */
/*                                                                           */
/*                                 NOTICE                                    */
/*                                                                           */
/* Permission to use, copy, modify, and distribute this software and         */
/* its documentation for any purpose and without fee is hereby granted       */
/* provided that the above notice appear in all copies and supporting        */
/* documentation.                                                            */
/*                                                                           */
/* Neither the Institutions (Institute of Computational Mathematics and      */
/* Scientific/Engineering Computing, Chinese Academy of Sciences) nor the    */
/* Authors make any representations about the suitability of this software   */
/* for any purpose.  This software is provided ``as is'' without expressed   */
/* or implied warranty.                                                      */
/*                                                                           */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/* C includes */
extern "C"{
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
}

/* local includes */
#include "llgrevpre_double.h"
#include "qsort_double.h"
#include "iotext_double.h"
#include "spblas.h"

template < class Real >
Real abs(Real x)
{
	return (x > 0 ? x : -x);
}

/* empty constructor */
CompCol_LLGrevPreconditioner_double::CompCol_LLGrevPreconditioner_double(void)
        : d_val_(0), Z_val_(0), Z_colptr_(0), Z_rowind_(0), Z_nz_(0),
	  V_val_(0), V_colptr_(0), V_rowind_(0), V_nz_(0)
{
	dim_[0] = 0;
    dim_[1] = 0;
}

/* main constructor */
CompCol_LLGrevPreconditioner_double::
CompCol_LLGrevPreconditioner_double(const CompCol_Mat_double &A, const double tau, const double taus, double& pretime)
: d_val_(0), Z_val_(0), Z_colptr_(0), Z_rowind_(0), Z_nz_(0),
	  V_val_(0), V_colptr_(0), V_rowind_(0), V_nz_(0)
{
	dim_[0] = A.dim(0);
	dim_[1] = A.dim(1);

	int i, j, k, g, h;
	int tempinti, tempintj;
	int NumDcol = 0;

	double temp;
	double last = 1.e-6;
	double znorm = 0.0, unorm = 0.0, afnorms = 0.0/*, ajnormMax = 0.0*/;   // norm
	double stheta; // scalar theta

	VECTOR_double u(dim_[0],0.0), temp_z_col(dim_[1], 0.0), ajnorm(dim_[1],0.0);

	int nzA = A.NumNonzeros();
	VECTOR_double AT_val_(nzA,0.0), coln(dim_[1],0.0), colm(dim_[0],0.0);
	VECTOR_int AT_rowind_(nzA,0), AT_colptr_(dim_[0]+1,0), count(dim_[0],0);
	int index = 0;
	for (i  = 0; i< dim_[1]; i++)
	{
		for (j = A.col_ptr(i); j<A.col_ptr(i+1); j++)
		{
			k = A.row_ind(j);
			count(k)++;
		}
	}


	for (j = 0; j<dim_[0] ; j++)
	{
		AT_colptr_(j+1) = AT_colptr_(j) + count(j);
		count(j) = 0;
	}

	for (i = 0; i<dim_[1]; i++)
	{
		for (j = A.col_ptr(i); j < A.col_ptr(i+1) ; j++)
		{
			k = A.row_ind(j);
			index = AT_colptr_(k)+count(k);
			AT_rowind_(index) = i;
			AT_val_(index) = A.val(j);
			count(k)++;
		}
	}

	// Estimate the number of the nonzero elements in ATA
	count.newsize(dim_[1]);
	for (i = 0; i<dim_[1]; i++)
		count(i) = -1;

	int nnzATA = 0;
	for (j = 0 ; j<dim_[1] ; j++)
	{
		for (i = A.col_ptr(j); i<A.col_ptr(j+1); i++)
		{
			k = A.row_ind(i);
			for (h = AT_colptr_(k); h<AT_colptr_(k+1); h++)
			{
				g = AT_rowind_(h);
				if (count(g) != j)
				{
					count(g) = j;
					nnzATA++;
				}
			}
		}
	}

	VECTOR_double ATA_val_(nnzATA,0.0);
	VECTOR_int ATA_colptr_(dim_[1]+1,0), ATA_rowind_(nnzATA,0);
	nnzATA = 0;

	// form ATA
	for (j = 0; j<dim_[1]; j++)
	{

		coln = 0.0; colm = 0.0;
		for ( k = A.col_ptr(j); k < A.col_ptr(j+1); k++)
		{	
			g = A.row_ind(k);
			colm(g) = A.val(k);
		}

		for (k = A.col_ptr(j); k<A.col_ptr(j+1); k++)
		{
			g = A.row_ind(k);
			for (h = AT_colptr_(g); h < AT_colptr_(g+1); h++)
			{
				tempinti = AT_rowind_(h);
				coln(tempinti) += colm(g)*AT_val_(h);
			}
		}

		for (k = 0; k < dim_[1]; k++)
		{
			if (abs(coln(k)) >= 1.e-16)
			{
				ATA_val_(nnzATA) = coln(k);
				ATA_rowind_(nnzATA++) = k;
			}
		}
		ATA_colptr_(j+1) = nnzATA;
	} 


	// Initialize Matrix Z
	Z_nz_ = ((dim_[1]+1)*dim_[1])/2;
	Z_val_.newsize(Z_nz_);
	Z_val_ = 0.0;
	Z_colptr_.newsize(dim_[1]+1);
	Z_rowind_.newsize(Z_nz_);
	Z_colptr_ = 0;
	Z_rowind_ = 0;

	// Initial Matrix V
	// Assume there are no more than 1000 linearly dependent columns
	VECTOR_double dV_val_(1000*dim_[0],0.0);
	VECTOR_int V_indx(dim_[1],0);

	// Matrix D^{-1} in vector form.
	d_val_.newsize(dim_[1]);
	d_val_= 0.0;

	VECTOR_double ataj_val_(dim_[1],0.0);
	VECTOR_int ataj_rowind_(dim_[1],0);
	int ataj_nz_ = 0;
	VECTOR_double aj_val_(dim_[0],0.0);
	VECTOR_int aj_rowind_(dim_[0],0);
	int aj_nz_=0;

	int descra[9];
      descra[0] = 0;
      descra[1] = 0;
      descra[2] = 0;

	VECTOR_double result(dim_[1], 0.0);
	VECTOR_double work(dim_[1]);
	VECTOR_double theta(dim_[1],0.0);

	for (i = 0; i<dim_[1] ; i++)
	{
		for (j = A.col_ptr(i); j<A.col_ptr(i+1); j++)
			ajnorm(i) += A.val(j)*A.val(j);
		ajnorm(i) = sqrt(ajnorm(i));
	}			

	// Start the timer
	last = clock();        

	d_val_(0) = ajnorm(0)*ajnorm(0);
	afnorms = d_val_(0);

	if ( fabs(ajnorm(0))> 1.e-10 ) 
	{
		d_val_(0)=1.0/d_val_(0);
	}
	else
	{
		return;
	}
 
	// construct Z(:,1);
	Z_val_(0) = 1.0;
	Z_nz_ = 1;
	Z_colptr_(0) = 0;
	Z_colptr_(1) = 1;
	Z_rowind_(0) = 0;
	znorm = 1.0;
	V_indx(0) = -1;


	for ( j=1 ; j<dim_[1]; j++)  // generated the jth column of Z and V 
	{	

		// compute aj^T*A 
		for (i = 0; i<j;)
		{
			temp_z_col(i++) = 0.0; //initialize
		}

		ataj_nz_ = 0;
		h = ATA_colptr_(j+1)-1;
		for (i = ATA_colptr_(j); i< h ;)
		{
			k = ATA_rowind_(i);
			if (k > j)
			{
				break;
			}
			ataj_val_(ataj_nz_) = ATA_val_(i++);
			ataj_rowind_(ataj_nz_++) = k;

			k = ATA_rowind_(i);
			if (k > j)
			{
				break;
			}
			ataj_val_(ataj_nz_) = ATA_val_(i++);
			ataj_rowind_(ataj_nz_++) = k;
		}
		if (h == i)
		{	
			k = ATA_rowind_(h);
			ataj_val_(ataj_nz_) = ATA_val_(h);
			ataj_rowind_(ataj_nz_++) = k;
		}

		aj_nz_=0;
		g = A.col_ptr(j+1);
		for (i = A.col_ptr(j); i<g; )
		{
			aj_val_(aj_nz_) = A.val(i);
			aj_rowind_(aj_nz_++) = A.row_ind(i++);
		}


		for (i = 0; i<j; i++)
		{	
			stheta = 0.0;
			if ( V_indx(i) < 0)// compute aj^TA * z_i
			{
				g = Z_colptr_(i);
				tempinti = Z_colptr_(i+1);
				for (k = 0; k<ataj_nz_; k++)
				{
					h = ataj_rowind_(k);
					if (h > i)
						break;
					for (; Z_rowind_(g)<= h && g < tempinti ; g++)
					{
						if ( Z_rowind_(g) == h)
						{
							stheta += ataj_val_(k)*Z_val_(g);
							break;
						}
					}
				}
			}
			else  // compute aj^T*v_i
			{
				g = V_indx(i)*dim_[0];
				tempinti = aj_nz_-1;
				for (k = 0; k < tempinti; )
				{
					h = g+aj_rowind_(k);
					stheta += aj_val_(k++) * dV_val_(h);
					h = g+aj_rowind_(k);
					stheta += aj_val_(k++) * dV_val_(h);
				}
				if ( tempinti == k)
				{
					h = g+aj_rowind_(k);
					stheta += aj_val_(k) * dV_val_(h);
				}
			}

			stheta *= d_val_(i);
			if (fabs(stheta)>1.e-10)
			{
				h = Z_colptr_(i+1)-1;
				for( k=Z_colptr_(i); k< h; k+=2 )
				{
					temp_z_col(Z_rowind_(k)) -= stheta * Z_val_(k);
					temp_z_col(Z_rowind_(k+1)) -= stheta * Z_val_(k+1);
				}
				if ( h==k)
					temp_z_col(Z_rowind_(k)) -= stheta * Z_val_(k);
			}
		}

		znorm = 0.0;
		for (k = 0; k<j; k++)
		{
			if (fabs(temp_z_col(k))>znorm)
			{
				znorm = fabs(temp_z_col(k));
			}
		}
		
		for (k=0; k<j ;k++)
		{
			temp = temp_z_col(k);
			if (fabs(temp) > tau * znorm)
			{
				Z_val_(Z_nz_) = temp;
				Z_rowind_(Z_nz_++) = k;
			}
			else
			{
				temp_z_col(k) = 0.0;
			}
		}
		Z_val_(Z_nz_) = 1.0;
		Z_rowind_(Z_nz_++) = j;
		Z_colptr_(j+1) = Z_nz_;

		u = 0.0;
		for (i = Z_colptr_(j); i<Z_colptr_(j+1) ; i++)
		{
			h = A.col_ptr(Z_rowind_(i) + 1) -1;
			for (k = A.col_ptr(Z_rowind_(i)); k< h; k+=2)
			{
				u(A.row_ind(k)) += Z_val_(i)*A.val(k);
				u(A.row_ind(k+1)) += Z_val_(i)*A.val(k+1);
			}
			if ( h == k)
			{
				u(A.row_ind(k)) += Z_val_(i)*A.val(k);
			}
		}
		unorm = 0.0;
		h = dim_[0]-1;
		for (k = 0; k< h; k+=2)
		{
			temp = u(k);
			if (fabs(temp) > 1.e-16 )
			{
				unorm += temp*temp;
			}
			temp = u(k+1);
			if (fabs(temp) > 1.e-16 )
			{
				unorm += temp*temp;
			}
		}
		if (h ==k)
		{
			temp = u(h);
			if (fabs(temp) > 1.e-16 )
			{
				unorm += temp*temp;
			}
		}	
		
		d_val_(j) = 1.0/unorm;

		if (sqrt(unorm) > taus * ajnorm(j) * sqrt(afnorms))
		{
			V_indx(j)--;
		}
		else
		{	
			V_indx(j) = NumDcol++;
			znorm = 0.0;
			g = Z_colptr_(j+1);
			for (k = Z_colptr_(j); k< g; )
			{
				temp = Z_val_(k++);
				znorm += temp * temp;
				temp = Z_val_(k++);
				znorm += temp * temp;
			}
			if (g == k)
			{	
				temp = Z_val_(k);
				znorm += temp * temp;
			}
			

			d_val_(j) = 1.0/(znorm);


			theta = 0.0;

			F77NAME(dcscmm) (1, j, 1, j, 1.0,
            descra, &Z_val_(0), &Z_rowind_(0), &Z_colptr_(0),
            &temp_z_col(1), j, 1.0, &theta(0), j,
            &work(0), j);

			tempintj = 0;
			temp_z_col = 0.0;
			for (i = 0; i<j; i++)
			{	
				theta(i) *= d_val_(i);
				if (V_indx(i)<0 && fabs(theta(i))>1.e-16)
				{
					g = Z_colptr_(i+1);
					for (k = Z_colptr_(i); k<g; k++)
					{
						temp_z_col(Z_rowind_(k)) -= theta(i)*Z_val_(k);
					}
				}
			}
			colm = A * temp_z_col;
			for (i = 0; i< j ; i++)
			{
				if (V_indx(i) >=0 && fabs(theta(i))>1.e-16)
				{
					tempintj = V_indx(i)*dim_[0];
					for (k = 0; k< dim_[0]; k++)
					{
						colm(k) -= theta(i)*dV_val_(tempintj+k);
					}
				}
			}
			tempintj = V_indx(j)*dim_[0];
			for (i = 0; i<dim_[0]; i++)
			{
				if (fabs(colm(i)) > 1.e-16)
				{
					dV_val_(tempintj + i) = colm(i);
				}
			}

		}
		afnorms += (ajnorm(j)*ajnorm(j));              
	}
	pretime = (clock()-last)/(CLOCKS_PER_SEC);

	/*post process*/
	int ii, nzD=0;
	for (ii=0;ii<dim_[1];ii++)
	{
		if (d_val_(ii)!=0)
		{
			nzD++;
		}
	}
	
	/*Rewrite V in to CompCol_Mat format*/
	V_nz_ = dim_[0]*dim_[1];
	V_val_.newsize(V_nz_);
	V_val_ = 0.0;
	V_colptr_.newsize(dim_[1]+1);
	V_rowind_.newsize(V_nz_);
	V_colptr_ = 0;
	V_rowind_ = 0;
	V_nz_ = 0;

	for ( j = 0; j<dim_[1]; j++)
	{	
		if (V_indx(j)< 0)
		{

			temp_z_col = 0.0;
			for (i = Z_colptr_(j); i<Z_colptr_(j+1); i++)
			{
				k = Z_rowind_(i);
				temp_z_col(k) = Z_val_(i);
			}
			colm = A*temp_z_col;
			for (i = 0; i<dim_[0]; i++)
			{
				if (fabs(colm(i))>1.e-16)
				{
					V_val_(V_nz_) = colm(i);
					V_rowind_(V_nz_++) = i;
				}
			}
			V_colptr_(j+1) = V_nz_;
		}
		else
		{
			for (i = 0; i < dim_[0]; i++)
			{	
				if ( fabs( dV_val_(V_indx(j)*dim_[0]+i) )>1.e-16 )
				{
					V_val_(V_nz_) = dV_val_(V_indx(j)*dim_[0]+i);
					V_rowind_(V_nz_++) = i;
				}
			}
			V_colptr_(j+1) = V_nz_;
		}
	}
}

/* compute the product between the preconditioner and the vector: M'x*/
VECTOR_double CompCol_LLGrevPreconditioner_double::Bsolve(const CompCol_Mat_double &A, const VECTOR_double &x) const  
{
	if (A.dim(0)<A.dim(1))
	{
		return CompCol_LLGrevPreconditioner_double::trans_solve(x);
	}
	else
	{
		return CompCol_LLGrevPreconditioner_double::solve(x);
	}
}

/* compute the product between the preconditioner and the vector: M'x = ZDV'x */
VECTOR_double CompCol_LLGrevPreconditioner_double::solve(const VECTOR_double &x) const 
{
	int M =dim_[0], N = dim_[1],i/*,j,k*/;
	VECTOR_double y(N,0.0),z(N,0.0), work(N);
	
	int descra[9];
	descra[0] = 0;
	descra[1] = 0;
	descra[2] = 0;
	
	//y=V'x
	F77NAME(dcscmm) (1, N, 1, M, 1.0,
					 descra, &V_val_(0), &V_rowind_(0), &V_colptr_(0),
				 &x(1), M, 1.0, &y(0), N,
				 &work(0), N);
	
	//y=Dy
	for (  i =0 ; i< dim_[1] ; i++)
	{
	y(i) *= d_val_(i);
	}
	
	//z=Zy
	F77NAME(dcscmm) (0, N, 1, N, 1.0,
		   descra, &Z_val_(0), &Z_rowind_(0), &Z_colptr_(0),
		   &y(0), N, 1.0, &z(1), N,
		   &work(1), N);
	
	return z;
} 

/* compute the product between the transpose of the preconditioner and the vector: M'x = VDZ'x */
VECTOR_double CompCol_LLGrevPreconditioner_double::trans_solve(const VECTOR_double &x) const 
{
	int M =dim_[0], N = dim_[1],i;
	VECTOR_double y(N,0.0),z(M,0.0), work(N);
	
	int descra[9];
	descra[0] = 0;
	descra[1] = 0;
	descra[2] = 0;
	
	//y=Z'x
	F77NAME(dcscmm) (1, N, 1, N, 1.0,
					 descra, &Z_val_(0), &Z_rowind_(0), &Z_colptr_(0),
				 &x(1), N, 1.0, &y(0), N,
				 &work(0), N);
	
	//y=Dy
	for (  i =0 ; i< N ; i++)
	{
	y(i) *= d_val_(i);
	}
	
	// upper unit
	//  descra[1] = 0;
	//  descra[2] = 0;
	work.newsize(M);
	work = 0.0;
	
	//z=Vy
	F77NAME(dcscmm) (0, M, 1, N, 1.0,
		   descra, &V_val_(0), &V_rowind_(0), &V_colptr_(0),
		   &y(0), N, 1.0, &z(1), M,
		   &work(1), M);
	
	return z;
}
	
/* get vector d_val */
double *CompCol_LLGrevPreconditioner_double::get_D_val(void) const
{
	double *d_val = (double *)calloc(d_val_.dim(),sizeof(double));
	int i;
	for (i=0;i<d_val_.dim();i++)
	{
		d_val[i] = d_val_(i);
	}
	return d_val;
}

/* get vector Z_val */
double *CompCol_LLGrevPreconditioner_double::get_Z_val(void) const
{
	double *Z_val = (double *)calloc(Z_val_.dim(),sizeof(double));
	int i;
	for (i=0;i<Z_val_.dim();i++)
	{
		Z_val[i] = Z_val_(i);
	}
	return Z_val;
}

/* get vector Z_colptr */
int *CompCol_LLGrevPreconditioner_double::get_Z_colptr(void) const
{
	int *Z_colptr = (int *)calloc(Z_colptr_.dim(),sizeof(int));
	int i;
	for (i=0;i<Z_colptr_.dim();i++)
	{
		Z_colptr[i] = Z_colptr_(i);
	}
	return Z_colptr;
}

/* get vector Z_rowind */
int *CompCol_LLGrevPreconditioner_double::get_Z_rowind(void) const
{
	int *Z_rowind = (int *)calloc(Z_rowind_.dim(),sizeof(int));
	int i;
	for (i=0;i<Z_rowind_.dim();i++)
	{
		Z_rowind[i] = Z_rowind_(i);
	}
	return Z_rowind;
}

/* get int Z_nz */
int CompCol_LLGrevPreconditioner_double::get_Z_nz(void) const
{
	return Z_nz_;
}

/* get vector V_val */
double *CompCol_LLGrevPreconditioner_double::get_V_val(void) const
{
	double *V_val = (double *)calloc(V_val_.dim(),sizeof(double));
	int i;
	for (i=0;i<V_val_.dim();i++)
	{
		V_val[i] = V_val_(i);
	}
	return V_val;
}

/* get vector V_colptr */
int *CompCol_LLGrevPreconditioner_double::get_V_colptr(void) const
{
	int *V_colptr = (int *)calloc(V_colptr_.dim(),sizeof(int));
	int i;
	for (i=0;i<V_colptr_.dim();i++)
	{
		V_colptr[i] = V_colptr_(i);
	}
	return V_colptr;
}

/* get vector V_rowind */
int *CompCol_LLGrevPreconditioner_double::get_V_rowind(void) const
{
	int *V_rowind = (int *)calloc(V_rowind_.dim(),sizeof(int));
	int i;
	for (i=0;i<V_rowind_.dim();i++)
	{
		V_rowind[i] = V_rowind_(i);
	}
	return V_rowind;
}

/* get int V_nz */
int CompCol_LLGrevPreconditioner_double::get_V_nz(void) const
{
	return V_nz_;
}

/* get vector dims */
const int *CompCol_LLGrevPreconditioner_double::get_dims(void) const
{
	return dim_;
}
