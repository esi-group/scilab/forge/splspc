/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* Copyright (C) 2007 - National Institute of Informatics - Jun-Feng Yin     */
/* Copyright (C) 2011 - DIGITEO - Michael Baudin                             */
/* Copyright (C) 2011 - National Institute of Informatics - Benoit Goepfert  */
/*                                                                           */
/* This file must be used under the terms of the CeCILL.                     */
/* This source file is licensed as described in the file COPYING, which      */
/* you should have received as part of this distribution.  The terms         */
/* are also available at                                                     */
/* http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                  */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*                                                                           */
/*                     A Package based on                                    */
/*                     SparseLib++ : Sparse Matrix Library                   */
/*                                                                           */
/*                                 NOTICE                                    */
/*                                                                           */
/* Permission to use, copy, modify, and distribute this software and         */
/* its documentation for any purpose and without fee is hereby granted       */
/* provided that the above notice appear in all copies and supporting        */
/* documentation.                                                            */
/*                                                                           */
/* Neither the Institutions (Institute of Computational Mathematics and      */
/* Scientific/Engineering Computing, Chinese Academy of Sciences) nor the    */
/* Authors make any representations about the suitability of this software   */
/* for any purpose.  This software is provided ``as is'' without expressed   */
/* or implied warranty.                                                      */
/*                                                                           */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

//C includes
extern "C"{
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
}

//local includes
#include "zrifpre_double.h"
#include "qsort_double.h"
#include "iotext_double.h"
#include "spblas.h"

/* empty constructor */
CompCol_ZRIFPreconditioner_double::CompCol_ZRIFPreconditioner_double(void)
: d_val_(0), sd_val_(0), z_val_(0), z_colptr_(0), z_rowind_(0), z_nz_(0)
{
	dim_[0] = 0;
	dim_[1] = 0;
}

/* main constructor */
CompCol_ZRIFPreconditioner_double::
CompCol_ZRIFPreconditioner_double(const CompCol_Mat_double &A, const double tau, double& pretime, int &cancel)
: d_val_(A.dim(1)), sd_val_(A.dim(1)), z_val_(0), z_colptr_(A.dim(1) + 1), z_rowind_(0), z_nz_(0)
{
	int i, j, k;
	double ONE = 1.0, ZERO = 0.0, temp, theta; 
	double last = 1.e-6; // factorization cpu time

	/*copy*/
	dim_[0] = A.dim(1);
	dim_[1] = A.dim(1);

	long int r_nz_ = (dim_[1]+1)*dim_[1]/2;
	VECTOR_double r_val_(r_nz_);
	VECTOR_double u(A.dim(0),0.0),v(A.dim(0),0.0);
	VECTOR_double cNorm(A.dim(1),0.0);

	for ( j=0 ; j<dim_[1] ; j++)
	{
		for ( i=0 ; i<j; i++ )
		{
			r_val_(j*(j+1)/2+i) = ZERO;
		}
		r_val_(j*(j+1)/2+i) = ONE;
	}
	for ( j=0 ; j<dim_[1] ; j++)
	{
		for ( i=A.col_ptr(j) ; i < A.col_ptr(j+1); i++ )
		{
			cNorm(j) += A.val(i)*A.val(i);
		}
		cNorm(j) = sqrt(cNorm(j));
	}
	last = clock();
	
	/*record time*/
	for ( j=0 ; j<dim_[1]; j++)//dim_[1] 
	{	
		u = ZERO;
		for (k=A.col_ptr(j); k<A.col_ptr(j+1); k++)
		{
			u(A.row_ind(k)) = A.val(k);
		}
		for ( i=0 ; i < j; i++)
		{
			if (fabs(r_val_(j*(j+1)/2+i)) > tau*cNorm(j))
			{				
				for (k=A.col_ptr(i); k<A.col_ptr(i+1); k++)
				{
					u(A.row_ind(k))+=r_val_(j*(j+1)/2+i)*A.val(k);
				}
			}
			
		}
		temp = dot(u,u);
		if ( fabs(temp) < 1e-16)
		{
			cancel=1;
			return ;
		}
		 else
		{
			d_val_(j) = 1/temp;
		}
		 
		for ( i=j+1; i<dim_[1]; i++)
		{
			theta = 0;
			for (k=A.col_ptr(i); k<A.col_ptr(i+1); k++)
			{
				theta += u(A.row_ind(k)) * A.val(k);
			}
			theta *= d_val_(j);
			if (fabs(theta) > tau*cNorm(i) )
			{
				for ( k = 0;  k < j+1 ; k++ )
				{
					r_val_(i*(i+1)/2+k) -= theta * r_val_(j*(j+1)/2+k);		
				}
			}
		}
	}
	pretime = (clock()-last)/(CLOCKS_PER_SEC);	

	/*post process*/
	for (i = 0; i < dim_[1]; i++)
	{
		sd_val_(i) = sqrt(d_val_(i));
	}
	r_nz_=0;
	for ( j=0 ; j<dim_[1] ; j++)
	{
		for ( i=0 ; i<j+1; i++ )
		{
			if ( fabs(r_val_(j*(j+1)/2+i)) > 1e-10 )
			{
				r_nz_++;
			}
		}
	}	
	z_nz_= r_nz_;
	z_val_.newsize(r_nz_);
	z_val_ = 0;
	z_rowind_.newsize(r_nz_);
	z_rowind_ = 0;
	z_colptr_(0) = 0;	
	for ( j=0 ; j<dim_[1] ; j++)
	{
		z_colptr_(j+1)=z_colptr_(j);
		for ( i=0 ; i<j+1; i++ )
		{
			if ( fabs(r_val_(j*(j+1)/2+i)) > 1e-10 )
			{
				k = z_colptr_(j+1)++;
				z_val_(k) = r_val_(j*(j+1)/2+i);
				z_rowind_(k) = i;
			}
		}
	}
	cancel=0;
}

/* compute the product between the preconditioner and the vector: M'x*/
VECTOR_double CompCol_ZRIFPreconditioner_double::Bsolve(const CompCol_Mat_double &A, const VECTOR_double &x) const  
{
	if (A.dim(0)<A.dim(1))
	{
		return CompCol_ZRIFPreconditioner_double::trans_solve(A, x);
	}
	else
	{
		return CompCol_ZRIFPreconditioner_double::solve(A, x);
	}
}

/* compute the product between the preconditioner and the vector: M'x = A'ZDZ'x */
VECTOR_double CompCol_ZRIFPreconditioner_double::trans_solve(const CompCol_Mat_double &A, const VECTOR_double &x) const 
{
	VECTOR_double w, v;
	
	//w=x
	w=x;
	
	int M = w.size(),i;
	VECTOR_double y(M,0.0),z(M,0.0);
	VECTOR_double work(M);	
	int descra[9];
	descra[0] = 0;	
	descra[1] = 0;
	descra[2] = 0;
	
	//y=Z'w
	F77NAME(dcscmm) (1, M, 1, M, 1.0,
		   descra, &z_val_(0), &z_rowind_(0), &z_colptr_(0),
		   &w(1), M, 1.0, &y(0), M,
		   &work(0), M);
	
	//y=Dy
	for (  i =0 ; i< M ; i++)
	{
		y(i) *= d_val_(i);
	}
	
	descra[1] = 0;
	descra[2] = 0;
	
	//z=Zy
	F77NAME(dcscmm) (0, M, 1, M, 1.0,
		   descra, &z_val_(0), &z_rowind_(0), &z_colptr_(0),
		   &y(0), M, 1.0, &z(1), M,
		   &work(1), M);
	
	//v=A'z
	v=A.trans_mult(z); 
					
	return v;
}

/* compute the product between the preconditioner and the vector: M'x = ZDZ'A'x */
VECTOR_double CompCol_ZRIFPreconditioner_double::solve(const CompCol_Mat_double &A, const VECTOR_double &x) const 
{
	VECTOR_double w, v;
		
	//w=A'x
	w=A.trans_mult(x);
		
	int M = w.size(),i;
	VECTOR_double y(M,0.0),z(M,0.0);
	VECTOR_double work(M);
	
	int descra[9];
	descra[0] = 0;	
	descra[1] = 0;
	descra[2] = 0;
	
	//y=Z'w
	F77NAME(dcscmm) (1, M, 1, M, 1.0,
		   descra, &z_val_(0), &z_rowind_(0), &z_colptr_(0),
		   &w(1), M, 1.0, &y(0), M,
		   &work(0), M);
	
	//y=Dy
	for (  i =0 ; i< M ; i++)
	{
		y(i) *= d_val_(i);
	}
	
	descra[1] = 0;
	descra[2] = 0;
	
	//z=Zy
	F77NAME(dcscmm) (0, M, 1, M, 1.0,
		   descra, &z_val_(0), &z_rowind_(0), &z_colptr_(0),
		   &y(0), M, 1.0, &z(1), M,
		   &work(1), M);
	
	//v=z
	v=z;	   
					
	return v;
}

/* get vector d_val */
double *CompCol_ZRIFPreconditioner_double::get_D_val(void)
{
	double *d_val = (double *)calloc(d_val_.dim(),sizeof(double));
	int i;
	for (i=0;i<d_val_.dim();i++)
	{
		d_val[i] = d_val_(i);
	}
	return d_val;
}

/* get vector sd_val */
double *CompCol_ZRIFPreconditioner_double::get_SD_val(void)
{
	double *sd_val = (double *)calloc(sd_val_.dim(),sizeof(double));
	int i;
	for (i=0;i<sd_val_.dim();i++)
	{
		sd_val[i] = sd_val_(i);
	}
	return sd_val;
}

/* get vector z_val */
double *CompCol_ZRIFPreconditioner_double::get_Z_val(void)
{
	double *z_val = (double *)calloc(z_val_.dim(),sizeof(double));
	int i;
	for (i=0;i<z_val_.dim();i++)
	{
		z_val[i] = z_val_(i);
	}
	return z_val;
}

/* get vector z_colptr */
int *CompCol_ZRIFPreconditioner_double::get_Z_colptr(void)
{
	int *z_colptr = (int *)calloc(z_colptr_.dim(),sizeof(int));
	int i;
	for (i=0;i<z_colptr_.dim();i++)
	{
		z_colptr[i] = z_colptr_(i);
	}
	return z_colptr;
}

/* get vector z_rowind */
int *CompCol_ZRIFPreconditioner_double::get_Z_rowind(void)
{
	int *z_rowind = (int *)calloc(z_rowind_.dim(),sizeof(int));
	int i;
	for (i=0;i<z_rowind_.dim();i++)
	{
		z_rowind[i] = z_rowind_(i);
	}
	return z_rowind;
}

/* get int z_nz */
int CompCol_ZRIFPreconditioner_double::get_Z_nz(void)
{
	return z_nz_;
}

/* get vector dims */
int *CompCol_ZRIFPreconditioner_double::get_dims(void)
{
	return dim_;
}

