// ====================================================================
// Copyright (C) 2011 - Benoit Goepfert
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================


src_dir   = get_absolute_file_path("builder_src.sce");
src_path  = "src";
linknames = ["niicpp"];
files = [
	  "zrifpre_double.cpp"
	  "llgrevpre_double.cpp"
  ];

ldflags = "";

if MSDOS then
  include1 = src_dir+"..\include";
  include2 = src_dir+"..\..\mv\include";
  include3 = src_dir+"..\..\sparselib\include";
  cflags = "-DWIN32 -DLIBSPLSPC_EXPORTS -I"""+...
		   include1+""" -I"""+include2+""" -I"""+include3+"""";
  libs     = [
			  "..\..\spblas\libspblas"
			  "..\..\mv\src\libmv"
			  "..\..\sparselib\src\libsparselib"
			  ];			  
else
  include1 = src_dir;
  include2 = src_dir+"../include";
  include3 = src_dir+"../../mv/include";
  include4 = src_dir+"../../sparselib/include";
  cflags = "-ansi -g -Wall -pedantic -O3 -I"""+...
		   include1+""" -I """+include2+""" -I """+include3+""" -I"""+include4+"""";
  libs = [
		  "../../spblas/libspblas"
		  "../../mv/src/libmv"
		  "../../sparselib/src/libsparselib"
		  ];		  
end

tbx_build_src(linknames, files, src_path, src_dir, libs, ldflags, cflags);

clear src_dir src_path linknames files ldflags tbx_build_src;

